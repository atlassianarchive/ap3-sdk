#!/bin/sh
SDK_FOLDER=$1
DEPLOY_FOLDER=target/deploy
INSTALLER_FOLDER=target/installer
mkdir -p $DEPLOY_FOLDER
cd $DEPLOY_FOLDER
git clone git@bitbucket.org:atlassian/ap3-sdk-bin.git ./ap3-sdk

cd ./ap3-sdk
find . -name ".git" -prune -o -exec rm -rf {} \;

echo "contents of new git dir:"
ls -al
cd ../../../
echo "currently in: " `pwd`
cp -R $INSTALLER_FOLDER/$SDK_FOLDER/* $DEPLOY_FOLDER/ap3-sdk/
cd $DEPLOY_FOLDER/ap3-sdk

git add -u .
git add .
git status
git commit -m "updating sdk installer"
git push origin master

