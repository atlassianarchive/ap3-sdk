#!/bin/sh
SDK_FOLDER=$1
TARGET_FOLDER=$2
INSTALLER_FOLDER=target/installer
BINDIR=$TARGET_FOLDER/.ap3-sdk/bin
rm -rf $TARGET_FOLDER/.ap3-sdk/
rm -rf ${HOME}/.ap3/
mkdir -p $TARGET_FOLDER/.ap3-sdk/
cp -R $INSTALLER_FOLDER/$SDK_FOLDER/* $TARGET_FOLDER/.ap3-sdk/
sudo rm /usr/local/bin/ap3
sudo ln -s ${BINDIR}/ap3 /usr/local/bin/ap3

cd $TARGET_FOLDER/.ap3-sdk
. ./ap3-setup-env.sh
. ./ap3-setup-config.sh

BASHIT_ALIASES=${HOME}/.bash_it/aliases
BASHIT_COMPLETE=${HOME}/.bash_it/completion

if [ ! -z "$BASHIT_ALIASES" ] ; then
    echo "found bash-it, copying..."
    rm -f $BASHIT_ALIASES/enabled/ap3.aliases.bash
    rm -f $BASHIT_ALIASES/available/ap3.aliases.bash
    cp $TARGET_FOLDER/.ap3-sdk/ap3.aliases.bash $BASHIT_ALIASES/available/
    ln -s $BASHIT_ALIASES/available/ap3.aliases.bash $BASHIT_ALIASES/enabled/ap3.aliases.bash
else
    echo "bash-it not found!"
fi

if [ -x "$BASHIT_COMPLETE" ] ; then
    echo "found bash-it, copying completions..."
    rm -f $BASHIT_COMPLETE/enabled/ap3.completion.bash
    rm -f $BASHIT_COMPLETE/available/ap3.completion.bash
    cp $TARGET_FOLDER/.ap3-sdk/ap3.completion.bash $BASHIT_COMPLETE/available/
    chmod +x $BASHIT_COMPLETE/available/ap3.completion.bash
    ln -s $BASHIT_COMPLETE/available/ap3.completion.bash $BASHIT_COMPLETE/enabled/ap3.completion.bash
    BASHIT_ENABLED=true
fi
