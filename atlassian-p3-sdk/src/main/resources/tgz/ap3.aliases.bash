cite 'about-alias'
about-alias 'common ap3 abbreviations'

# Aliases
alias ap3d='export AP3_OPTS="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"'
alias ap3dx='export AP3_OPTS='
alias ap3c='ap3 config'
alias ap3cg='ap3 config -g'
alias ap3n='ap3 new'
alias ap3ns='ap3 new servlet-kit'
alias ap3nr='ap3 new ringo-kit'
alias ap3i='ap3 init'
alias ap3ir='ap3 init ringo-kit'
alias ap3is='ap3 init servlet-kit'
alias ap3sp3='ap3 start p3'
alias ap3sj='ap3 start jira'
alias ap3sc='ap3 start confluence'

function ap3-help() {
  echo "AP3 Custom Aliases Usage"
  echo
  echo "  ap3d	  = debug opts on for debugging SDK"
  echo "  ap3dx   = debug opts off for debugging SDK"
  echo "  ap3c 	  = ap3 config"
  echo "  ap3cg   = ap3 config -g"
  echo "  ap3n	  = ap3 new"
  echo "  ap3ns   = ap3 new servlet-kit"
  echo "  ap3nr	  = ap3 new ringo-kit"
  echo "  ap3i	  = ap3 init"
  echo "  ap3is   = ap3 init servlet-kit"
  echo "  ap3ir	  = ap3 init ringo-kit"
  echo "  ap3sp3  = ap3 start p3"
  echo "  ap3sj  = ap3 start jira"
  echo "  ap3sc  = ap3 start confluence"
  echo
}

