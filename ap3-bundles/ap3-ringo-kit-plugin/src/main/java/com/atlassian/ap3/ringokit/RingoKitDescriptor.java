package com.atlassian.ap3.ringokit;

import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.spi.kit.KitDescriptor;

@Named
@Singleton
public class RingoKitDescriptor implements KitDescriptor
{
    public static final String KIT_ID = "ringo-kit";
    public static final String TEMPLATE_REPO_URL = "https://bitbucket.org/atlassian/ap3-ringo-kit-templates.git";
    
    @Override
    public String getKitId()
    {
        return KIT_ID;
    }

    @Override
    public String getTemplatesRepoUrl()
    {
        return TEMPLATE_REPO_URL;
    }
}
