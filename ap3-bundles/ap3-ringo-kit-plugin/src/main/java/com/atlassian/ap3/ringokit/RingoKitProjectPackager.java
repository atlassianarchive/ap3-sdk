package com.atlassian.ap3.ringokit;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.JarAndVersion;
import com.atlassian.ap3.api.JarResolver;
import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.kit.BundlePackageBuilder;
import com.atlassian.ap3.api.kit.ProjectPackage;
import com.atlassian.ap3.api.maven.MavenPomHelper;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.spi.kit.ProjectPackager;
import com.atlassian.plugin.remotable.descriptor.PolyglotDescriptorAccessor;

import org.apache.commons.lang.StringUtils;

import static com.google.common.collect.Lists.newArrayList;

@Named
@Singleton
@RequiresKit(RingoKitDescriptor.class)
public class RingoKitProjectPackager implements ProjectPackager
{
    private final HomeLocator homeLocator;
    private final JarResolver jarResolver;
    private final MavenPomHelper pomHelper;
    private final Prompter prompter;

    @Inject
    public RingoKitProjectPackager(HomeLocator homeLocator, JarResolver jarResolver, MavenPomHelper pomHelper, Prompter prompter)
    {
        this.homeLocator = homeLocator;
        this.jarResolver = jarResolver;
        this.pomHelper = pomHelper;
        this.prompter = prompter;
    }

    @Override
    public ProjectPackage packageProject(Path projectRoot, Path containerJar, String containerVersion, List<String> jvmArgs) throws IOException
    {

        String bundledLibs = generateBundledLibsEntry(projectRoot);
        Path pomxml = projectRoot.resolve("pom.xml");

        String ringoVersion = "LATEST";
        String ringoKitVersion = "LATEST";

        if (Files.exists(pomxml))
        {
            String ringoPomVersion = pomHelper.getPropertyValue(pomxml, "ringojs.version");
            String ringoKitPomVersion = pomHelper.getPropertyValue(pomxml, "ringokit.version");

            if (StringUtils.isNotBlank(ringoPomVersion))
            {
                ringoVersion = ringoPomVersion;
            }

            if (StringUtils.isNotBlank(ringoKitPomVersion))
            {
                ringoKitVersion = ringoKitPomVersion;
            }
        }

        JarAndVersion ringoJav = jarResolver.resolveArtifact("org.twdata.org.ringojs", "ringojs", ringoVersion);
        JarAndVersion ringoKitJav = jarResolver.resolveArtifact("com.atlassian.pluginkit", "ringojs-kit", ringoKitVersion);


        BundlePackageBuilder bundleBuilder = new BundlePackageBuilder(projectRoot);
        if (null != ringoJav && Files.exists(ringoJav.getJar()))
        {
            bundleBuilder.addJar(ringoJav.getJar());
        }
        else
        {
            try
            {
                prompter.showWarning("ringojs.jar not found! not adding to plugin jar.");
            }
            catch (PrompterException e)
            {
                //ignore
            }
        }

        if (null != ringoKitJav && Files.exists(ringoKitJav.getJar()))
        {
            bundleBuilder.addJar(ringoKitJav.getJar());
        }
        else
        {
            try
            {
                prompter.showWarning("ringojs-kit.jar not found! not adding to plugin jar.");
            }
            catch (PrompterException e)
            {
                //ignore
            }
        }

        if (StringUtils.isNotBlank(bundledLibs))
        {
            bundleBuilder.addManifestEntry("Bundle-ClassPath", bundledLibs);
        }

        Path plugin = bundleBuilder.build();

        return new ProjectPackage(projectRoot, projectRoot, getPluginKey(projectRoot), plugin);

    }

    private String getPluginKey(Path projectRoot)
    {
        PolyglotDescriptorAccessor descriptorAccessor = new PolyglotDescriptorAccessor(projectRoot.toAbsolutePath().toFile());

        return descriptorAccessor.getDescriptor().getRootElement().attributeValue("key");
    }

    private String generateBundledLibsEntry(Path projectRoot) throws IOException
    {
        final Path libDir = projectRoot.resolve("lib");
        if (Files.exists(libDir))
        {
            final List<String> entries = newArrayList();
            entries.add(".");

            Files.walkFileTree(libDir, new SimpleFileVisitor<Path>()
            {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {
                    entries.add("lib/" + libDir.relativize(file).toString());
                    return FileVisitResult.CONTINUE;
                }

            });


            return StringUtils.join(entries, ',');
        }

        return "";
    }

    private Path extractFileFromContainer(FileSystem zip, String entry, String filename) throws IOException
    {
        Path containerDir = homeLocator.getContainerDirectory();
        Path jarFile = containerDir.resolve(filename);
        if (Files.notExists(jarFile))
        {
            Path entryFile = zip.getPath(entry);
            Files.copy(entryFile, jarFile, StandardCopyOption.REPLACE_EXISTING);
        }

        return jarFile;
    }

}
