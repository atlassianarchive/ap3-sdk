package ut.com.atlassian.ap3.api.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("simple")
public class SimpleConfigEntry
{
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
