package ut.com.atlassian.ap3.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since version
 */
public class RegexTest
{
    public static final Pattern HOST_PATTERN = Pattern.compile("([^:]+)[:]([^@]+)[@](https://|http://)?([^:/]+)([:]([0-9]+))?([/](.*))?");
    
    @Test
    public void testWorks() throws Exception
    {
        Matcher m1 = HOST_PATTERN.matcher("user:pass@localhost");
        Matcher m2 = HOST_PATTERN.matcher("user:pass@localhost:880");
        Matcher m3 = HOST_PATTERN.matcher("user:pass@localhost:880/junk");
        Matcher m4 = HOST_PATTERN.matcher("user:pass@localhost/junk");
        Matcher m5 = HOST_PATTERN.matcher("user:pass@localhost:80/");
        Matcher m6 = HOST_PATTERN.matcher("user:pass@localhost/");
        Matcher m7 = HOST_PATTERN.matcher("user:pass@http://localhost");
        Matcher m8 = HOST_PATTERN.matcher("user:pass@http://localhost:880");
        Matcher m9 = HOST_PATTERN.matcher("user:pass@http://localhost:880/junk");
        Matcher m10 = HOST_PATTERN.matcher("user:pass@http://localhost/junk");
        Matcher m11 = HOST_PATTERN.matcher("user:pass@http://localhost:80/");
        Matcher m12 = HOST_PATTERN.matcher("user:pass@http://localhost/");
        Matcher m13 = HOST_PATTERN.matcher("user:pass@https://localhost");
        Matcher m14 = HOST_PATTERN.matcher("user:pass@https://localhost:880");
        Matcher m15 = HOST_PATTERN.matcher("user:pass@https://localhost:880/junk");
        Matcher m16 = HOST_PATTERN.matcher("user:pass@https://localhost/junk");
        Matcher m17 = HOST_PATTERN.matcher("user:pass@https://localhost:80/");
        Matcher m18 = HOST_PATTERN.matcher("user:pass@https://localhost/");
        assertTrue(m1.matches());
        assertTrue(m2.matches());
        assertTrue(m3.matches());
        assertTrue(m4.matches());
        assertTrue(m5.matches());
        assertTrue(m6.matches());
        assertTrue(m7.matches());
        assertTrue(m8.matches());
        assertTrue(m9.matches());
        assertTrue(m10.matches());
        assertTrue(m11.matches());
        assertTrue(m12.matches());
        assertTrue(m13.matches());
        assertTrue(m14.matches());
        assertTrue(m15.matches());
        assertTrue(m16.matches());
        assertTrue(m17.matches());
        assertTrue(m18.matches());
    }

    @Test
    public void testFails() throws Exception
    {
        Matcher m1 = HOST_PATTERN.matcher("user@localhost");
        Matcher m2 = HOST_PATTERN.matcher("user@localhost:80");
        Matcher m3 = HOST_PATTERN.matcher(":pass@localhost");
        Matcher m4 = HOST_PATTERN.matcher("user:pass@localhost:");
        Matcher m5 = HOST_PATTERN.matcher("user:pass@localhost:jkgfd");
        Matcher m6 = HOST_PATTERN.matcher("user:pass@localhost:a80");
        Matcher m7 = HOST_PATTERN.matcher("user:pass@localhost:/context");
        Matcher m8 = HOST_PATTERN.matcher("user:pass@localhost:80context");
        Matcher m9 = HOST_PATTERN.matcher("user:pass@https:localhost");
        Matcher m10 = HOST_PATTERN.matcher("user:pass@http:/localhost");
        Matcher m11 = HOST_PATTERN.matcher("user:pass@http/localhost");
        Matcher m12 = HOST_PATTERN.matcher("user:pass@https/localhost");
        Matcher m13 = HOST_PATTERN.matcher("user:pass@http//localhost");
        Matcher m14 = HOST_PATTERN.matcher("user:pass@httpd://localhost:80");
        

        assertFalse(m1.matches());
        assertFalse(m2.matches());
        assertFalse(m3.matches());
        assertFalse(m4.matches());
        assertFalse(m5.matches());
        assertFalse(m6.matches());
        assertFalse(m7.matches());
        assertFalse(m8.matches());
        assertFalse(m9.matches());
        assertFalse(m10.matches());
        assertFalse(m14.matches());
    }

    @Test
    public void testFind() throws Exception
    {
        Matcher m1 = HOST_PATTERN.matcher("user:pass@localhost");
        Matcher m2 = HOST_PATTERN.matcher("user:pass@http://localhost:880/junk");


        m1.matches();
        assertEquals("user",m1.group(1));
        assertEquals("pass",m1.group(2));
        assertEquals(null,m1.group(3));
        assertEquals("localhost",m1.group(4));
        assertEquals(null,m1.group(6));
        assertEquals(null,m1.group(7));
        assertEquals(null,m1.group(8));

        m2.matches();
        assertEquals("user",m2.group(1));
        assertEquals("pass",m2.group(2));
        assertEquals("http://",m2.group(3));
        assertEquals("localhost",m2.group(4));
        assertEquals("880",m2.group(6));
        assertEquals("/junk",m2.group(7));

    }
}
