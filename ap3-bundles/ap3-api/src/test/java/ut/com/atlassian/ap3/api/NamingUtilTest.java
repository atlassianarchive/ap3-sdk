package ut.com.atlassian.ap3.api;

import com.atlassian.ap3.api.util.NamingUtil;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since version
 */
public class NamingUtilTest
{
    @Test
    public void lowerCamelToDash() throws Exception
    {
        String expected = "my-plugin";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("myPlugin");
        
        assertEquals(expected,actual);
    }

    @Test
    public void cappedCamelToDash() throws Exception
    {
        String expected = "my-plugin";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("MyPlugin");

        assertEquals(expected,actual);
    }

    @Test
    public void cappedAlphaNumericToDash() throws Exception
    {
        String expected = "p3";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("P3");

        assertEquals(expected,actual);
    }

    @Test
    public void lowerAlphaNumericToDash() throws Exception
    {
        String expected = "p3";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("p3");

        assertEquals(expected,actual);
    }

    @Test
    public void lowerSpaceToDash() throws Exception
    {
        String expected = "my-plugin";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("my plugin");

        assertEquals(expected,actual);
    }

    @Test
    public void cappedSpaceToDash() throws Exception
    {
        String expected = "my-plugin";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("My Plugin");

        assertEquals(expected,actual);
    }

    @Test
    public void mixedSpaceToDash() throws Exception
    {
        String expected = "my-plugin";
        String actual = NamingUtil.camelCaseOrSpaceToDashed("My plugin");

        assertEquals(expected,actual);
    }
}
