package ut.com.atlassian.ap3.api.config;

/**
 * @since version
 */
public class UnlabledConfigEntry
{
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
