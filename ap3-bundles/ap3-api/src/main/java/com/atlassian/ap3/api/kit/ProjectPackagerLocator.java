package com.atlassian.ap3.api.kit;

import com.atlassian.ap3.spi.kit.ProjectPackager;

import com.google.inject.ImplementedBy;

@ImplementedBy(ProjectPackagerLocatorImpl.class)
public interface ProjectPackagerLocator
{
    ProjectPackager getPackager(String kitId);
}
