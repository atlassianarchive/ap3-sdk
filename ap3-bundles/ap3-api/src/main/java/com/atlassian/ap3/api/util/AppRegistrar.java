package com.atlassian.ap3.api.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.bind.DatatypeConverter;

import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;

import org.apache.commons.io.IOUtils;

@Named
@Singleton
public class AppRegistrar
{
    private final Prompter prompter;

    @Inject
    public AppRegistrar(Prompter prompter)
    {
        this.prompter = prompter;
    }

    public void registerApp(AppRegistrationDetails registrationDetails) throws IOException, PrompterException
    {
        URI host = registrationDetails.getHost();
        String appKey = registrationDetails.getAppKey();
        String username = registrationDetails.getUsername();
        String password = registrationDetails.getPassword();
        
        URL url = new URL(host.toString() + "/rest/remotable-plugins/latest/installer");
        OutputStream out = null;
        try
        {
            String mountUrl = registrationDetails.getLocalBaseUrl() + "/" + appKey;
            String creds = username + ":" + password;
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setDoOutput(true);
            uc.setDoInput(true);
            String authorizationString = "Basic " + DatatypeConverter.printBase64Binary(creds.getBytes(Charset.defaultCharset()));
            uc.setRequestProperty("Authorization", authorizationString);
            uc.setRequestMethod("POST");
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            out = uc.getOutputStream();
            out.write(
                    ("url=" + URLEncoder.encode(mountUrl, "UTF-8") + "&token=").getBytes(Charset.defaultCharset()));
            out.close();
            int status = uc.getResponseCode();
            InputStream is;
            if (status >= 400)
            {
                is = uc.getErrorStream();
                prompter.showError("Registration of '" + appKey + "' at '" + host + "' failed:\n" + IOUtils.toString(is));
            }
            else
            {
                is = uc.getInputStream();
                prompter.showInfo("Registered '" + appKey + "' at '" + host + "'");
            }
            is.close();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    public void unregisterApp(AppRegistrationDetails registrationDetails) throws IOException, PrompterException
    {
        URI host = registrationDetails.getHost();
        String appKey = registrationDetails.getAppKey();
        String username = registrationDetails.getUsername();
        String password = registrationDetails.getPassword();
        
        URL url = new URL(host.toString() + "/rest/remotable-plugins/latest/uninstaller/" + appKey);

        String creds = username + ":" + password;
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setDoOutput(true);
        uc.setDoInput(true);
        String authorizationString = "Basic " + DatatypeConverter.printBase64Binary(creds.getBytes(Charset.defaultCharset()));
        uc.setRequestProperty("Authorization", authorizationString);
        uc.setRequestMethod("DELETE");
        uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        uc.connect();
        int status = uc.getResponseCode();
        InputStream is;
        if (status >= 400)
        {
            is = uc.getErrorStream();
            prompter.showError("Uninstallation of '" + appKey + "' at '" + host + "' failed:\n" + IOUtils.toString(is));
        }
        else
        {
            is = uc.getInputStream();
            prompter.showInfo("Uninstalled '" + appKey + "' at '" + host + "'");
        }
        is.close();

    }
}
