package com.atlassian.ap3.api.util;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @since version
 */
public class LaunchBrowser
{
    public static void launch(String url)
    {
        if(Desktop.isDesktopSupported())
        {
            Desktop desktop = Desktop.getDesktop();
            if(desktop.isSupported(Desktop.Action.BROWSE))
            {
                try
                {
                    URI uri = new URI(url);
                    desktop.browse(uri);
                }
                catch (URISyntaxException | IOException e)
                {
                   //ignore
                }
            }
        }
    }
}
