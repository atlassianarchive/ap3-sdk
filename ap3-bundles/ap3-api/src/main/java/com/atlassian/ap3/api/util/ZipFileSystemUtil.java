package com.atlassian.ap3.api.util;

import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * @since version
 */
public class ZipFileSystemUtil
{
    public static FileSystem createZipFileSystem(Path zipPath) throws IOException
    {
        URI uri = URI.create("jar:file:" + zipPath.toUri().getPath());
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        
        return FileSystems.newFileSystem(uri, env);
    }

    public static FileSystem openZipFileSystem(Path zipPath) throws IOException
    {
        URI uri = URI.create("jar:file:" + zipPath.toUri().getPath());
        
        return FileSystems.newFileSystem(uri, Maps.<String,String> newHashMap());
    }
    
    public static void unzip(Path zipFile, final Path toDir) throws IOException
    {
        try(FileSystem srcZip = ZipFileSystemUtil.openZipFileSystem(zipFile))
        {
            Files.walkFileTree(srcZip.getPath("/"), new SimpleFileVisitor<Path>()
            {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
                {
                    Files.createDirectories(toDir.resolve(dir.toString()));
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {
                    Path dest = toDir.resolve(file.toString());
                    if (Files.notExists(dest))
                    {
                        Files.copy(file, dest);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }
}
