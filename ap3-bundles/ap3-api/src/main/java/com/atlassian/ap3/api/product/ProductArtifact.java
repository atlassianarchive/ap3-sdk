package com.atlassian.ap3.api.product;

/**
 * @since version
 */
public class ProductArtifact
{
    private final String groupId;
    private final String artifactId;
    private final String versionOrRange;

    public ProductArtifact(String groupId, String artifactId, String versionOrRange)
    {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.versionOrRange = versionOrRange;
    }

    public ProductArtifact(String groupId, String artifactId)
    {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.versionOrRange = "";
    }

    public String getGroupId()
    {
        return groupId;
    }

    public String getArtifactId()
    {
        return artifactId;
    }

    public String getVersionOrRange()
    {
        return versionOrRange;
    }
}
