package com.atlassian.ap3.api.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.util.NIOFileUtils;
import com.atlassian.ap3.ext.gson.Ap3ConfigEntryObjectAdapterFactory;
import com.atlassian.ap3.home.HomeLocator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang3.StringUtils;

@Named
public final class Ap3ConfigurationManager
{
    public static final String GLOBAL_CONFIG_FILENAME = "config";
    public static final String PROJECT_CONFIG_FILENAME = ".ap3";

    private final HomeLocator homeLocator;
    private final ConfigurationEntryTypeRegistry configTypeRegistry;

    private Ap3Configuration globalConfig;

    @Inject
    public Ap3ConfigurationManager(HomeLocator homeLocator, ConfigurationEntryTypeRegistry configTypeRegistry)
    {
        this.homeLocator = homeLocator;
        this.configTypeRegistry = configTypeRegistry;
    }

    public <T> T getGlobalConfigurationEntry(Class<T> type)
    {
        Ap3Configuration config = loadGlobalConfigurationIfNeeded();

        if (null != config)
        {
            return config.getConfigurationEntry(type);
        }

        return null;
    }

    public <T> T getProjectConfigurationEntry(Class<T> type, Path projectRoot)
    {
        Ap3Configuration config = loadProjectConfigurationIfNeeded(projectRoot);

        if (null != config)
        {
            return config.getConfigurationEntry(type);
        }

        return null;
    }

    public void saveGlobalEntry(Object configEntry) throws IOException
    {
        Ap3Configuration config = loadGlobalConfigurationIfNeeded();

        if (null != config)
        {
            config.updateConfigurationEntry(configEntry);

            Path configFile = getGlobalConfigFile();

            if (Files.notExists(configFile))
            {
                Files.createFile(configFile);
            }

            Gson gson = createPrettyGson();
            String json = gson.toJson(config);
            
            NIOFileUtils.writeStringToFile(configFile, json);
        }

    }

    public void removeGlobalEntry(Object configEntry) throws IOException
    {
        Ap3Configuration config = loadGlobalConfigurationIfNeeded();

        if (null != config)
        {
            config.removeConfigurationEntry(configEntry);

            Path configFile = getGlobalConfigFile();

            if (Files.notExists(configFile))
            {
                Files.createFile(configFile);
            }

            Gson gson = createPrettyGson();
            String json = gson.toJson(config);

            NIOFileUtils.writeStringToFile(configFile, json);
        }

    }

    public void saveProjectEntry(Object configEntry, Path projectRoot) throws IOException
    {
        Ap3Configuration config = loadProjectConfigurationIfNeeded(projectRoot);

        if (null != config)
        {
            config.updateConfigurationEntry(configEntry);

            Path configFile = getProjectConfigFile(projectRoot);

            if (Files.notExists(configFile))
            {
                Files.createFile(configFile);
            }

            Gson gson = createPrettyGson();
            String json = gson.toJson(config);
            NIOFileUtils.writeStringToFile(configFile, json);
        }

    }

    public void removeProjectEntry(Object configEntry, Path projectRoot) throws IOException
    {
        Ap3Configuration config = loadProjectConfigurationIfNeeded(projectRoot);

        if (null != config)
        {
            config.removeConfigurationEntry(configEntry);

            Path configFile = getProjectConfigFile(projectRoot);

            if (Files.notExists(configFile))
            {
                Files.createFile(configFile);
            }

            Gson gson = createPrettyGson();
            String json = gson.toJson(config);
            NIOFileUtils.writeStringToFile(configFile, json);
        }

    }

    private Ap3Configuration loadGlobalConfigurationIfNeeded()
    {
        Ap3Configuration config = null;
        Path configFile = getGlobalConfigFile();

        if (null == configFile)
        {
            return null;
        }

        globalConfig = loadConfiguration(configFile);
        config = globalConfig;

        return config;
    }

    private Ap3Configuration loadProjectConfigurationIfNeeded(Path projectRoot)
    {
        Path configFile = getProjectConfigFile(projectRoot);

        if (null == configFile)
        {
            return null;
        }

        return loadConfiguration(configFile);

    }

    private Path getGlobalConfigFile()
    {
        Path configFile = null;

        try
        {
            configFile = homeLocator.getConfigDirectory().resolve(GLOBAL_CONFIG_FILENAME);
        }
        catch (Exception e)
        {
            //do nothing
        }

        return configFile;
    }

    private Path getProjectConfigFile(Path projectRoot)
    {
        Path configFile = null;

        try
        {
                configFile = projectRoot.resolve(PROJECT_CONFIG_FILENAME);
        }
        catch (Exception e)
        {
            //do nothing
        }

        return configFile;
    }

    private Ap3Configuration loadConfiguration(Path configFile)
    {
        Ap3Configuration loadedConfig = null;
        try
        {
            if (Files.notExists(configFile))
            {
                Files.write(configFile, "{}".getBytes(), StandardOpenOption.CREATE);
            }

            String configJson = NIOFileUtils.readFileToString(configFile);
            
            if (StringUtils.isBlank(configJson))
            {
                Files.write(configFile, "{}".getBytes(), StandardOpenOption.CREATE);
            }

            Gson gson = createGson();

            loadedConfig = gson.fromJson(configJson, Ap3Configuration.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            loadedConfig = null;
        }

        return loadedConfig;
    }

    private Gson createGson()
    {
        GsonBuilder gsonBuilder = createGsonBuilder();
        Gson gson = gsonBuilder.create();

        return gson;
    }

    private Gson createPrettyGson()
    {
        GsonBuilder gsonBuilder = createGsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();

        return gson;
    }

    private GsonBuilder createGsonBuilder()
    {
        Ap3ConfigEntryObjectAdapterFactory adapter = new Ap3ConfigEntryObjectAdapterFactory();
        adapter.registerConfigurationTypes(configTypeRegistry.getEntryTypes());
        adapter.registerConfigurationType(UnknownConfigurationEntry.class);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(adapter);

        return gsonBuilder;
    }

}
