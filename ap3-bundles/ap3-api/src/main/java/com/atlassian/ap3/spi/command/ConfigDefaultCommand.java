package com.atlassian.ap3.spi.command;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.cli.Ap3Cli;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.eclipse.sisu.Nullable;

import io.airlift.command.Command;
import io.airlift.command.Group;
import io.airlift.command.Option;
import io.airlift.command.OptionType;
import io.airlift.command.model.CommandGroupMetadata;
import io.airlift.command.model.CommandMetadata;

@Named
@Group(name = "config", description = "Configures the Atlassian Plugins 3 SDK", defaultCommand = ConfigDefaultCommand.class)
@Command(name = "config-default", hidden = true)
public final class ConfigDefaultCommand extends BaseAp3Command
{
    @Option(name = {"-g", "--global"}, title = "Global Configuration", description = "Global configuration vs. Project configuration", type = OptionType.GROUP)
    private boolean global = false;
    
    
    @Inject
    @Nullable
    private CommandGroupMetadata groupMetadata;

    private final Prompter prompter;
    private final Ap3Cli cli;

    @Inject
    public ConfigDefaultCommand(Prompter prompter, Ap3Cli cli)
    {
        this.prompter = prompter;
        this.cli = cli;
    }

    @Override
    public void run() throws Ap3Exception
    {
        Path projectFile = Paths.get("").resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);

        if(!global && Files.notExists(projectFile))
        {
            try
            {
                prompter.showError("Current directory is not a valid AP3 project.");
                prompter.showWarning("Either re-run this command with the -g|--global flag, or run: ap3 init");
                return;
            }
            catch (PrompterException e)
            {
                //do nothing
            }
        }
        
        if(null != groupMetadata)
        {
            CommandMetadata metadata = promptForConfigCommand(groupMetadata.getCommands());
            Ap3Command command = cli.getParsedCommand(metadata.getType());

            if(null != command)
            {
                command.run();
            }
        }
    }

    private CommandMetadata promptForConfigCommand(List<CommandMetadata> commands) throws Ap3Exception
    {
        CommandMetadata command = null;

        Collection<CommandMetadata> choices = Collections2.filter(commands, new Predicate<CommandMetadata>()
        {
            @Override
            public boolean apply(CommandMetadata input)
            {
                return !input.isHidden();
            }
        });

        try
        {
            command = prompter.promptForChoice("Choose a configuration", choices, new Function<CommandMetadata, String>() {
                @Override
                public String apply(CommandMetadata input)
                {
                    return input.getName();
                }
            });
        }
        catch (PrompterException e)
        {
            throw new Ap3Exception("Error prompting for configuration type", e);
        }

        return command;
    }
        
}
