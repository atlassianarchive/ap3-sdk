package com.atlassian.ap3.api.kit;

import java.util.Set;

import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.inject.ImplementedBy;

@ImplementedBy(KitLocatorDescriptorImpl.class)
public interface KitDescriptorLocator
{
    <T extends KitDescriptor> T getKitDescriptor(Class<T> kitClass);
    <T extends KitDescriptor> T getKitDescriptorById(String kitId);
    Set<KitDescriptor> getAllKitDescriptors();
}
