package com.atlassian.ap3.spi.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.ap3.api.config.*;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;

import com.google.common.base.Function;

import io.airlift.command.Group;

@Group(name = "remote", description = "Configures available remote hosts", defaultCommand = RemoteListCommand.class)
public abstract class AbstractRemoteCommand extends BaseAp3Command
{
    @Inject
    protected Ap3ConfigurationManager configurationManager;

    @Inject
    protected Prompter prompter;
    
    @Inject
    protected RemotesConfigurationHelper remotesHelper;

    protected List<RemoteHostConfigurationEntry> getRemotes()
    {
        RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
        if(null == remotesConfig)
        {
            return new ArrayList<>();
        }
        
        return remotesConfig.getRemotes();
    }
    
    protected void saveRemote(RemoteHostConfigurationEntry remote) throws IOException
    {
        Map<String,RemoteHostConfigurationEntry> map = remotesHelper.getRemotesAsMap();
        map.put(remote.getId(),remote);

        RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
        if(null == remotesConfig)
        {
            remotesConfig = new RemotesConfigurationEntry();    
        }
        
        remotesConfig.setRemotes(new ArrayList(map.values()));
        
        configurationManager.saveGlobalEntry(remotesConfig);

        addProjectDefaultIfNeeded(remote);
    }

    private void addProjectDefaultIfNeeded(RemoteHostConfigurationEntry newRemote) throws IOException
    {
        Path projectRoot = Paths.get("");
        Path projectFile = projectRoot.resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);
        
        if (Files.exists(projectFile))
        {
            DefaultRemoteConfigurationEntry currentDefault = configurationManager.getProjectConfigurationEntry(DefaultRemoteConfigurationEntry.class,projectRoot);
            if(null == currentDefault)
            {
                DefaultRemoteConfigurationEntry defaultRemote = new DefaultRemoteConfigurationEntry();
                defaultRemote.setId(newRemote.getId());

                configurationManager.saveProjectEntry(defaultRemote,projectRoot);
            }
        }
    }

    protected void deleteRemoteEntry(RemoteHostConfigurationEntry entry) throws IOException
    {
        Map<String,RemoteHostConfigurationEntry> map = remotesHelper.getRemotesAsMap();
        if(map.containsKey(entry.getId()))
        {
            map.remove(entry.getId());
        }

        RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
        List<RemoteHostConfigurationEntry> remotes;
        if(map.size() < 1)
        {
            remotes = new ArrayList<>();
        }
        else
        {
            remotes = new ArrayList<>(map.values());
        }
        
        if(null == remotesConfig)
        {
            remotesConfig = new RemotesConfigurationEntry();
        }
        
        remotesConfig.setRemotes(remotes);
        configurationManager.saveGlobalEntry(remotesConfig);
        
    }

    protected RemoteHostConfigurationEntry promptForRemoteFromList() throws PrompterException
    {
        return remotesHelper.promptForRemoteFromList();
    }

    protected RemoteHostConfigurationEntry getRemoteById(String id)
    {
        return remotesHelper.getRemoteById(id);
    }
    
}
