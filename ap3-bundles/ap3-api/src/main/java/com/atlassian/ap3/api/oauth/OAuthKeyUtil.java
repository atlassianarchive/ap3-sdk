package com.atlassian.ap3.api.oauth;

import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.inject.Named;
import javax.inject.Singleton;

import org.bouncycastle.openssl.PEMWriter;

@Named
@Singleton
public class OAuthKeyUtil
{
    public OAuthKeyPair generateKeys() throws NoSuchAlgorithmException, IOException
    {
        KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
        KeyPair pair = gen.generateKeyPair();
        
        StringWriter publicKeyWriter = new StringWriter();
        String pubKey = "";
        String pvtKey = "";
        
        try(PEMWriter pubWriter = new PEMWriter(publicKeyWriter)) 
        {
            pubWriter.writeObject(pair.getPublic());
            pubWriter.close();
            
            pubKey = publicKeyWriter.toString();
        }

        StringWriter privateKeyWriter = new StringWriter();
        try(PEMWriter privWriter = new PEMWriter(privateKeyWriter);)
        {
            privWriter.writeObject(pair.getPrivate());
            privWriter.close();

            pvtKey = privateKeyWriter.toString();
        }

        return new OAuthKeyPair(pvtKey,pubKey);
    }
    
    public String getOAuthXmlBlock(OAuthKeyPair keyPair)
    {
        return "<oauth>\n" +
                "    <public-key>\n        " +
                keyPair.getPublicKey() +
                "    </public-key>\n" +
                "</oauth>";
    }
}
