package com.atlassian.ap3.api.kit;

import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.common.collect.ImmutableSet;

@Named
@Singleton
public class KitLocatorDescriptorImpl implements KitDescriptorLocator
{
    private final Map<String, KitDescriptor> kits;

    @Inject
    public KitLocatorDescriptorImpl(Map<String, KitDescriptor> kits)
    {
        this.kits = kits;
    }

    @Override
    public <T extends KitDescriptor> T getKitDescriptor(final Class<T> kitClass)
    {
        for(Map.Entry<String,KitDescriptor> entry : kits.entrySet())
        {
            KitDescriptor kit = entry.getValue();
            if(kit.getClass().getName().equals(kitClass.getName()))
            {
                return (T) kit;
            }
        }
        
        return null;
    }

    @Override
    public <T extends KitDescriptor> T getKitDescriptorById(String kitId)
    {
        for(Map.Entry<String,KitDescriptor> entry : kits.entrySet())
        {
            KitDescriptor kit = entry.getValue();
            if(kit.getKitId().equals(kitId))
            {
                return (T) kit;
            }
        }

        return null;
    }

    @Override
    public Set<KitDescriptor> getAllKitDescriptors()
    {
        return ImmutableSet.copyOf(kits.values());
    }
}
