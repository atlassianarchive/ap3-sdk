package com.atlassian.ap3.spi.command;

import com.atlassian.ap3.api.Ap3Exception;

/**
 * @since version
 */
public interface Ap3Command
{
    void run() throws Ap3Exception;
    void setArgs(String[] args);
}
