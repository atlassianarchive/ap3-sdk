package com.atlassian.ap3.api.product;

import com.google.common.base.Preconditions;

/**
 * @since version
 */
public class ConfigFileReplacement
{
    private final String key;
    private final String value;

    /**
     * Replace the key with the value when unzipping a home. This is the normal meaning of
     * a replacement, {@code key -> value}
     */
    private final boolean applyWhenUnzipping;

    /**
     * Detect the value and replace it with the key when zipping a home directory
     */
    private final boolean applyWhenZipping;

    /**
     * Represents a key to be replaced in the configuration files.
     * <p/>
     * <p/>
     * <b>Important</b>: If your value is short, such as "/", "", "true", "false", please set reversible=false.
     * When zipping a home, config files are parsed and everything is replaced back with keys, such as %PRODUCT_HOME_DIR%.
     * If you provide a string with false positives, you may parametrise too many variables.
     *
     * @param key   the key to be replaced. Must not be null.
     * @param value the value to be replaced. Must not be null. <b>Important</b>: If short, such as / or "", please set reversible=false.
     */
    public ConfigFileReplacement(String key, String value)
    {
        this(key, value, true);
    }

    /**
     * Represents a key to be replaced in the configuration files.
     *
     * @param key        the key to be replaced. Must not be null.
     * @param value      the value to be replaced. Must not be null.
     * @param applyWhenZipping true if the value should be replaced with the key before
     *                   preparing a snapshot. Default is true. Use false when:<ul>
     *                   <li>the value is non-unique, e.g. "%BAMBOO_ENABLED% = true" should not be reversible.</li>
     *                   <li>we only support the value for legacy, but we wouldn't re-wrap a snapshot with this key</li>
     *                   </ul>
     */
    public ConfigFileReplacement(String key, String value, boolean applyWhenZipping)
    {
        this(key, value, true, applyWhenZipping);
    }

    /**
     * @param key                the key, never null
     * @param value              the value, never null
     * @param applyWhenUnzipping apply when unzipping a home. Defaults to true.
     * @param applyWhenZipping   apply when zipping a home. Defaults to true.
     */
    ConfigFileReplacement(String key, String value, boolean applyWhenUnzipping, boolean applyWhenZipping)
    {
        Preconditions.checkArgument(key != null, "key must not be null");
        Preconditions.checkArgument(value != null, "value must not be null");
        this.key = key;
        this.value = value;
        this.applyWhenUnzipping = applyWhenUnzipping;
        this.applyWhenZipping = applyWhenZipping;
    }

    public static ConfigFileReplacement onlyWhenCreatingSnapshot(String key, String value)
    {
        return new ConfigFileReplacement(key, value, true, false);
    }

    /**
     * @return the key to be replaced. Never null.
     */
    public String getKey()
    {
        return key;
    }

    /**
     * @return the value. Never null.
     */
    public String getValue()
    {
        return value;
    }

    public boolean applyWhenZipping()
    {
        return applyWhenZipping;
    }

    public boolean applyWhenUnzipping()
    {
        return applyWhenUnzipping;
    }

    @Override
    public String toString()
    {
        String operation;

        if (applyWhenUnzipping && applyWhenZipping)
        {
            operation = " <-> ";
        }
        else if (applyWhenUnzipping && !applyWhenZipping)
        {
            operation = " -> ";
        }
        else if (!applyWhenUnzipping && applyWhenZipping)
        {
            operation = " <- ";
        }
        else // !applyWhenUnzipping && !applyWhenZipping
        {
            operation = " (nop) ";
        }

        return key + operation + value;
    }
}