package com.atlassian.ap3.api;

import java.nio.file.Path;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.maven.MavenArtifactDownloader;

import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.resolution.ArtifactResult;

@Named
@Singleton
public class JarResolver
{
    private final MavenArtifactDownloader artifactDownloader;

    @Inject
    public JarResolver(MavenArtifactDownloader artifactDownloader)
    {
        this.artifactDownloader = artifactDownloader;
    }

    public JarAndVersion resolveLatestArtifact(String groupId,String artifactId)
    {
        JarAndVersion jav = null;
        
        ArtifactResult result = artifactDownloader.downloadArtifact(groupId, artifactId, "LATEST", "jar");
    
        if (null != result)
        {
            Artifact artifact = result.getArtifact();
            Path repoFile = artifact.getFile().toPath();
            
            jav = new JarAndVersion(repoFile,artifact.getVersion());
        }
        
        return jav;
    }
    
    public JarAndVersion resolveArtifact(String groupId,String artifactId, String version)
    {
        JarAndVersion jav = null;

        ArtifactResult result = artifactDownloader.downloadArtifact(groupId, artifactId, version, "jar");

        if (null != result)
        {
            Artifact artifact = result.getArtifact();
            Path repoFile = artifact.getFile().toPath();

            jav = new JarAndVersion(repoFile,artifact.getVersion());
        }

        return jav;
    }
    
}
