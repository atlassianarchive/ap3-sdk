package com.atlassian.ap3.api.maven;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;

import com.google.common.collect.Lists;

import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.repository.RepositoryPolicy;
import org.eclipse.aether.resolution.*;
import org.eclipse.aether.transfer.AbstractTransferListener;
import org.eclipse.aether.transfer.TransferCancelledException;
import org.eclipse.aether.transfer.TransferEvent;

@Named
@Singleton
public final class MavenArtifactDownloader
{
    private final RepositorySystem repositorySystem;
    private final Prompter prompter;

    @Inject
    public MavenArtifactDownloader(RepositorySystem repositorySystem, Prompter prompter)
    {
        this.repositorySystem = repositorySystem;
        this.prompter = prompter;
    }

    public ArtifactResult downloadArtifact(String groupId, String artifactId, String version)
    {
        return downloadArtifact(groupId, artifactId, version, "jar");
    }

    public ArtifactResult downloadArtifact(String groupId, String artifactId, String version, String extension)
    {
        return downloadArtifact(groupId, artifactId, version, extension, "");
    }

    public ArtifactResult downloadArtifact(String groupId, String artifactId, String version, String extension, String classifier)
    {
        
        ArtifactResult result = null;
        DefaultRepositorySystemSession session = new DefaultRepositorySystemSession();
        session.setTransferListener(new AbstractTransferListener()
        {
            @Override
            public void transferProgressed(TransferEvent event) throws TransferCancelledException
            {
                try
                {
                    long tbytes = event.getTransferredBytes();
                    String msgLength = "";
                    if (tbytes < 1024)
                    {
                        msgLength = tbytes + " bytes transfered.";
                    }
                    else if (tbytes < 1048576)
                    {
                        msgLength = (tbytes / 1024) + " kb transfered.";
                    }
                    else if (tbytes < 1073741824)
                    {
                        msgLength = (tbytes / 1048576) + " mb transfered.";
                    }
                    else
                    {
                        msgLength = (tbytes / 1073741824) + " gb transfered.";
                    }
                    prompter.replaceMessage(msgLength);
                }
                catch (PrompterException e)
                {

                }
            }

            @Override
            public void transferSucceeded(TransferEvent event)
            {
                try
                {
                    prompter.replaceMessage("");
                }
                catch (PrompterException e)
                {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }

            @Override
            public void transferFailed(TransferEvent event)
            {
                try
                {
                    prompter.replaceMessage("");
                }
                catch (PrompterException e)
                {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });

        LocalRepository localRepository = new LocalRepository(System.getProperty("user.home") + File.separator + ".m2" + File.separator + "repository");
        session.setLocalRepositoryManager(repositorySystem.newLocalRepositoryManager(localRepository));

        RemoteRepository.Builder remoteRepoBuilder = new RemoteRepository.Builder("atlassian-public", "default", "https://maven.atlassian.com/content/groups/public/")
                .setPolicy(new RepositoryPolicy(true, RepositoryPolicy.UPDATE_POLICY_ALWAYS, RepositoryPolicy.CHECKSUM_POLICY_IGNORE));

        /*
        if (version.contains("SNAPSHOT"))
        {
            remoteRepoBuilder.setSnapshotPolicy(new RepositoryPolicy(true, RepositoryPolicy.UPDATE_POLICY_ALWAYS, RepositoryPolicy.CHECKSUM_POLICY_IGNORE));
        }
        else
        {
            remoteRepoBuilder.setSnapshotPolicy(new RepositoryPolicy(false, RepositoryPolicy.UPDATE_POLICY_NEVER, RepositoryPolicy.CHECKSUM_POLICY_IGNORE));
        }
    */
        RemoteRepository remoteRepo = remoteRepoBuilder.build();

        Artifact artifact = new DefaultArtifact(groupId, artifactId, classifier, extension, version);

        try
        {
            if (version.startsWith("[") || version.startsWith("("))
            {
                VersionRangeRequest rangeRequest = new VersionRangeRequest();
                rangeRequest.setArtifact(artifact);
                rangeRequest.setRepositories(Lists.newArrayList(remoteRepo));

                VersionRangeResult rangeResult = repositorySystem.resolveVersionRange(session, rangeRequest);
                if (rangeResult.getVersions().isEmpty())
                {
                    return null;
                }

                artifact = new DefaultArtifact(groupId, artifactId, classifier, extension, rangeResult.getHighestVersion().toString());
            }

            ArtifactRequest request = new ArtifactRequest();
            request.setArtifact(artifact);
            request.setRepositories(Lists.newArrayList(remoteRepo));


            result = repositorySystem.resolveArtifact(session, request);
        }
        catch (ArtifactResolutionException e)
        {
            e.printStackTrace();
        }
        catch (VersionRangeResolutionException e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
