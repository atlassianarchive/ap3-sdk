package com.atlassian.ap3.api.cargo;

import org.codehaus.cargo.util.internal.log.AbstractLogger;
import org.codehaus.cargo.util.log.LogLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Ap3CargoLogger extends AbstractLogger
{
    private Logger logger;

    public Ap3CargoLogger(String category)
    {
        this.logger = LoggerFactory.getLogger(category);
    }

    @Override
    protected void doLog(LogLevel level, String message, String category)
    {
        if (level == LogLevel.WARN)
        {
            this.logger.warn(message);
        }
        else if (level == LogLevel.DEBUG)
        {
            this.logger.debug(message);
        }
        else
        {
            this.logger.info(message);
        }
    }
}
