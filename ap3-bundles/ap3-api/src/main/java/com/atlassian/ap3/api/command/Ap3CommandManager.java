package com.atlassian.ap3.api.command;

import java.util.Set;

import com.atlassian.ap3.spi.command.Ap3Command;

import com.google.inject.ImplementedBy;

@ImplementedBy(Ap3CommandManagerImpl.class)
public interface Ap3CommandManager
{
    Set<Ap3Command> getAllCommands();

    <T> Set<T> getCommands(final Class<? extends Ap3Command> clazz);

    Iterable<Class<? extends Ap3Command>> getCommandClasses();

    Ap3Command getCommandByName(String name);

    Ap3Command getCommandByType(Class<?> type);
}
