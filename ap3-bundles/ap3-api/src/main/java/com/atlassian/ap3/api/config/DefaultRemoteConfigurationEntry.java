package com.atlassian.ap3.api.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("defaultremote")
public class DefaultRemoteConfigurationEntry
{
    private String id;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}
