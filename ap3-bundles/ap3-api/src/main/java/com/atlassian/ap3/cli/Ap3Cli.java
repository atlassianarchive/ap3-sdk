package com.atlassian.ap3.cli;

import com.atlassian.ap3.spi.command.Ap3Command;

public interface Ap3Cli
{

    void run(String[] args);
    
    Ap3Command getParsedCommand(String name);
    Ap3Command getParsedCommand(Class<?> type);
    public Ap3Command getParsedCommand(Class<?> type, String[] args);
}
