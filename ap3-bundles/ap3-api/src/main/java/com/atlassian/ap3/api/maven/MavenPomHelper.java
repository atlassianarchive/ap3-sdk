package com.atlassian.ap3.api.maven;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.maven.model.Model;
import org.apache.maven.model.building.*;

@Named
@Singleton
public class MavenPomHelper
{
    private final ModelBuilder modelBuilder;

    public MavenPomHelper()
    {
        DefaultModelBuilderFactory factory = new DefaultModelBuilderFactory();
        this.modelBuilder = factory.newInstance();
    }

    public String getP3VersionProperty(Path pomXml) throws IOException
    {
        return getPropertyValue(pomXml,"p3.version");
    }

    public String getPropertyValue(Path pomXml, String propertyName) throws IOException
    {
        Model model = readPom(pomXml);

        return model.getProperties().getProperty(propertyName);
    }
    
    
    public Model readPom(Path pomXml) throws IOException
    {
        Model model;
        try
        {
            ModelBuildingRequest modelRequest = new DefaultModelBuildingRequest();
            modelRequest.setValidationLevel(ModelBuildingRequest.VALIDATION_LEVEL_MINIMAL);
            modelRequest.setProcessPlugins(false);
            modelRequest.setTwoPhaseBuilding(false);
            modelRequest.setModelSource(new FileModelSource(pomXml.toFile()));

            model = modelBuilder.build(modelRequest).getEffectiveModel();
        }
        catch (ModelBuildingException e)
        {
            throw new IOException("error reading pom.xml", e);
        }

        return model;
    }
    
}
