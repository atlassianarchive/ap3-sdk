package com.atlassian.ap3.api.reload;

import java.io.IOException;
import java.nio.file.*;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.util.AppRegistrar;
import com.atlassian.ap3.api.util.AppRegistrationDetails;

@Named
@Singleton
public class DescriptorFileWatcher
{
    //the watch service needs to be run in it's own thread
    private final Map<String, ExecutorService> watcherExecutors;
    private final AppRegistrar appRegistrar;
    private final Prompter prompter;

    @Inject
    public DescriptorFileWatcher(AppRegistrar appRegistrar, Prompter prompter)
    {
        this.appRegistrar = appRegistrar;
        this.prompter = prompter;
        this.watcherExecutors = new HashMap<>();
    }
    
    public void start(final AppRegistrationDetails registrationDetails, final Path resourcesDir) throws IOException
    {
        String appKey = registrationDetails.getAppKey();
        if(Files.exists(resourcesDir))
        {
            stop(registrationDetails);

            final ExecutorService exec = Executors.newSingleThreadExecutor();
            watcherExecutors.put(appKey,exec);
            
            final WatchService watchService = FileSystems.getDefault().newWatchService();
            resourcesDir.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
            
            Runnable task = new Runnable()
            {
                public void run()
                {
                    startWatching(registrationDetails, watchService, exec);
                }
            };
    
            exec.execute(task);
        }
    }
    
    public void stop(AppRegistrationDetails registrationDetails)
    {
        String appKey = registrationDetails.getAppKey();
        if(watcherExecutors.containsKey(appKey))
        {
            ExecutorService oldExec = watcherExecutors.get(appKey);
            oldExec.shutdown();
            watcherExecutors.remove(appKey);
        }
    }

    private void startWatching(final AppRegistrationDetails registrationDetails, final WatchService watchService, final ExecutorService exec)
    {
        while (!exec.isShutdown())
        {
            WatchKey signalledKey;
            try
            {
                signalledKey = watchService.take();
            }
            catch (InterruptedException ix)
            {
                // we'll ignore being interrupted
                continue;
            }
            catch (ClosedWatchServiceException cwse)
            {
                // other thread closed watch service
                break;
            }

            // get list of events from key
            List<WatchEvent<?>> list = signalledKey.pollEvents();

            // VERY IMPORTANT! call reset() AFTER pollEvents() to allow the
            // key to be reported again by the watch service
            signalledKey.reset();

            for (WatchEvent e : list)
            {
                String filenameFound = ((Path) e.context()).getFileName().toString();
                if(e.kind().equals(StandardWatchEventKinds.ENTRY_MODIFY) && filenameFound.startsWith("atlassian-plugin."))
                {
                    try
                    {
                        prompter.showWarning("Plugin descriptor changed, re-registering app with remote host...");
                        appRegistrar.registerApp(registrationDetails);
                    }
                    catch (IOException | PrompterException e1)
                    {
                        //ignore
                    }
                }
            }
        }

        try
        {
            watchService.close();
        }
        catch (IOException e)
        {
            //ignore
        }
    }
}
