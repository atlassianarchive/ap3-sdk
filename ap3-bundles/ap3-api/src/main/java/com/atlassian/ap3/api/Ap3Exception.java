package com.atlassian.ap3.api;

/**
 * @since version
 */
public class Ap3Exception extends Exception
{
    public Ap3Exception()
    {
    }

    public Ap3Exception(String message)
    {
        super(message);
    }

    public Ap3Exception(String message, Throwable cause)
    {
        super(message, cause);
    }

    public Ap3Exception(Throwable cause)
    {
        super(cause);
    }
}
