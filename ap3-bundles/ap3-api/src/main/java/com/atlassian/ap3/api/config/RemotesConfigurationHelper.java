package com.atlassian.ap3.api.config;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.cli.Ap3Cli;
import com.atlassian.ap3.spi.command.Ap3Command;
import com.atlassian.ap3.spi.command.RemoteAddCommand;

import com.google.common.base.Function;

import org.apache.commons.lang3.StringUtils;

@Named
@Singleton
public class RemotesConfigurationHelper
{
    public static final String NEW_HOST_CHOICE_ID = "!--- new remote host ---!";
    private final Ap3ConfigurationManager configurationManager;
    private final Prompter prompter;
    private final Ap3Cli cli;
    
    @Inject
    public RemotesConfigurationHelper(Ap3ConfigurationManager configurationManager, Prompter prompter, Ap3Cli cli)
    {
        this.configurationManager = configurationManager;
        this.prompter = prompter;
        this.cli = cli;
    }
    
    public RemoteHostConfigurationEntry findOrPromptForRemoteHost(Path projectRoot, String remoteId, String warnMessage) throws IOException
    {
        RemoteHostConfigurationEntry remoteHostConfig = findRemoteHost(projectRoot,remoteId);
        
        if (null == remoteHostConfig)
        {
            try 
            {
                RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
                
                if(null != remotesConfig && !remotesConfig.getRemotes().isEmpty())
                {
                    List<RemoteHostConfigurationEntry> choices = new ArrayList<>();
                    RemoteHostConfigurationEntry newChoice = new RemoteHostConfigurationEntry();
                    newChoice.setId(NEW_HOST_CHOICE_ID);
                    newChoice.setUrl("");
                    choices.add(newChoice);
                    choices.addAll(remotesConfig.getRemotes());

                    RemoteHostConfigurationEntry chosen = prompter.promptForChoice("Choose a remote host",choices,new Function<RemoteHostConfigurationEntry, String>() {
                        @Override
                        public String apply(RemoteHostConfigurationEntry input)
                        {
                            return "[" + input.getId() + "] " + input.getUrl();
                        }
                    });
                    
                    if(NEW_HOST_CHOICE_ID.equals(chosen.getId()))
                    {
                        Ap3Command cmd = cli.getParsedCommand(RemoteAddCommand.class);
                        cmd.run();
                        chosen = getProjectDefault(projectRoot);
                    }
                    else
                    {
                        DefaultRemoteConfigurationEntry defaultRemote = new DefaultRemoteConfigurationEntry();
                        defaultRemote.setId(chosen.getId());

                        configurationManager.saveProjectEntry(defaultRemote,projectRoot);
                    }
                    
                    if(null != chosen)
                    {
                        remoteHostConfig = chosen;
                    }
                }
                
                if(null == remoteHostConfig)
                {
                    if(StringUtils.isNotBlank(warnMessage))
                    {
                        prompter.showWarning(warnMessage);
                    }
                    
                    boolean configureRemote = prompter.promptForBoolean("Would you like to configure a remote host now?","Y");
    
                    if (configureRemote)
                    {
                        Ap3Command addCommand = cli.getParsedCommand(RemoteAddCommand.class);
                        if (null != addCommand)
                        {
                            addCommand.run();
                            remoteHostConfig = findRemoteHost(projectRoot,remoteId);
                        }
                    }
                }
            }
            catch (Ap3Exception e)
            {
                //ignore
            }
        }
        
        return remoteHostConfig;
    }

    public RemoteHostConfigurationEntry findRemoteHost(Path projectRoot, String remoteId)
    {

        RemoteHostConfigurationEntry hostConfig = null;

        if (StringUtils.isNotBlank(remoteId))
        {
            hostConfig = getRemoteById(remoteId);
        }

        if (null == hostConfig)
        {
            hostConfig = getProjectDefault(projectRoot);
        }

        return hostConfig;
    }
    
    public RemoteHostConfigurationEntry promptForRemoteFromList() throws PrompterException
    {
        RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
        if(null == remotesConfig || remotesConfig.getRemotes().isEmpty())
        {
            prompter.showWarning("No remotes are setup.");
            prompter.showMessage("run: 'ap3 remote add' to add a new remote host.");
            return null;
        }

        return prompter.promptForChoice("Choose a remote host",remotesConfig.getRemotes(),new Function<RemoteHostConfigurationEntry, String>() {
            @Override
            public String apply(RemoteHostConfigurationEntry input)
            {
                return "[" + input.getId() + "] " + input.getUrl();
            }
        });
    }

    public RemoteHostConfigurationEntry getRemoteById(final String id)
    {
        return getRemotesAsMap().get(id);
    }
    
    public Map<String,RemoteHostConfigurationEntry> getRemotesAsMap()
    {
        Map<String, RemoteHostConfigurationEntry> map = new HashMap<>();
        RemotesConfigurationEntry remotesConfig = configurationManager.getGlobalConfigurationEntry(RemotesConfigurationEntry.class);
        if(null != remotesConfig)
        {
            for(RemoteHostConfigurationEntry entry : remotesConfig.getRemotes())
            {
                map.put(entry.getId(),entry);
            }
        }

        return map;
    }
    
    public RemoteHostConfigurationEntry getProjectDefault(Path projectRoot)
    {
        DefaultRemoteConfigurationEntry defaultHost = configurationManager.getProjectConfigurationEntry(DefaultRemoteConfigurationEntry.class,projectRoot);
        if(null != defaultHost)
        {
            return getRemoteById(defaultHost.getId());
        }

        return null;
    }
}
