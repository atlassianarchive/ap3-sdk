package com.atlassian.ap3.api.util;

import java.net.URI;

/**
 * @since version
 */
public class AppRegistrationDetails
{
    private final URI host;
    private final String appKey;
    private final String localBaseUrl;
    private final String username;
    private final String password;

    public AppRegistrationDetails(URI host, String appKey, String localBaseUrl, String username, String password)
    {
        this.host = host;
        this.appKey = appKey;
        this.localBaseUrl = localBaseUrl;
        this.username = username;
        this.password = password;
    }

    public URI getHost()
    {
        return host;
    }

    public String getAppKey()
    {
        return appKey;
    }

    public String getLocalBaseUrl()
    {
        return localBaseUrl;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }
}
