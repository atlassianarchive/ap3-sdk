package com.atlassian.ap3.spi.config;

import com.atlassian.ap3.spi.command.Ap3Command;

/**
 * @since version
 */
public interface Ap3ConfigurationCommand<T> extends Ap3Command
{
    Class<T> getEntryType();
}
