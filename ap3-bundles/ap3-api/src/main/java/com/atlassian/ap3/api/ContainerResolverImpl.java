package com.atlassian.ap3.api;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.maven.MavenArtifactDownloader;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


//TODO: find a way to inject the offline flag and use local cache only
//TODO: use force update to clean cache dir and get a new jar
@Named
@Singleton
public class ContainerResolverImpl implements ContainerResolver
{
    private static final String LATEST_FILE = "latest";
    private static final String UPDATE_FILE = "lastupdate";
    private static final String CONTAINER_JAR_PREFIX = "remotable-plugins-container-";
    private static final String CONTAINER_JAR_SUFFIX = "-standalone.jar";
    private static final String CONTAINER_GROUPID = "com.atlassian.plugins";
    private static final String CONTAINER_ARTIFACTID = "remotable-plugins-container";
    private static final String CONTAINER_CLASSIFIER = "standalone";

    private final MavenArtifactDownloader artifactDownloader;

    @Inject
    public ContainerResolverImpl(MavenArtifactDownloader artifactDownloader)
    {
        this.artifactDownloader = artifactDownloader;
    }

    @Override
    public String getLatestContainerVersion() throws IOException
    {
        return getLatestContainerVersion(false);
    }

    @Override
    public String getLatestContainerVersion(boolean forceUpdate) throws IOException
    {

        Pair<Path, String> container = downloadLatestContainer(false);

        if (null != container)
        {
            return container.getRight();
        }

        return null;
    }


    @Override
    public Path getLatestContainerJar() throws IOException
    {
        return getLatestContainerJar(false);
    }

    @Override
    public Path getLatestContainerJar(boolean forceUpdate) throws IOException
    {
        Pair<Path, String> container = downloadLatestContainer(true);

        if (null != container)
        {
            return container.getLeft();
        }

        return null;
    }

    @Override
    public Path getContainerJar(String version, boolean forceUpdate) throws IOException
    {

        Pair<Path, String> container = downloadContainer(version, true);

        if (null != container)
        {
            return container.getLeft();
        }


        return null;
    }

    private Pair<Path, String> downloadLatestContainer(boolean showMessage) throws IOException
    {
        return downloadContainer("LATEST",showMessage);
    }

    private Pair<Path, String> downloadContainer(String version, boolean showMessage) throws IOException
    {
        if(showMessage)
        {
            System.out.println("Downloading P3 container...");
        }
        ArtifactResult result = artifactDownloader.downloadArtifact(CONTAINER_GROUPID, CONTAINER_ARTIFACTID, version, "jar", CONTAINER_CLASSIFIER);

        if (null != result)
        {
            Artifact artifact = result.getArtifact();
            Path repoFile = artifact.getFile().toPath();

            return Pair.of(repoFile, artifact.getVersion());
        }

        return null;
    }

    private void updateLastUpdateFile(Path containerDir) throws IOException
    {
        Path updateFile = containerDir.resolve(UPDATE_FILE);

        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();

        Files.write(updateFile, fmt.print(new DateTime()).getBytes(), StandardOpenOption.CREATE);
    }

    private void updateLatestFile(Path containerDir, String version) throws IOException
    {
        Path latestFile = containerDir.resolve(LATEST_FILE);
        Files.write(latestFile, version.getBytes(), StandardOpenOption.CREATE);
    }

    private boolean shouldUpdate(Path containerDir) throws IOException
    {
        Path updateFile = containerDir.resolve(UPDATE_FILE);
        if (Files.notExists(updateFile))
        {
            return true;
        }

        String ts = new String(Files.readAllBytes(updateFile));

        if (StringUtils.isBlank(ts))
        {
            return true;
        }

        DateTime now = new DateTime();
        DateTime lastUpdate = new DateTime(ts);

        if (Days.daysBetween(lastUpdate, now).getDays() > 1)
        {
            return true;
        }

        return false;
    }

    private Version getLatestVersionFromJars(Path containerDir) throws IOException
    {
        JarVisitor visitor = new JarVisitor();

        Files.walkFileTree(containerDir, EnumSet.noneOf(FileVisitOption.class), 2, visitor);

        return visitor.getLatestVersion();
    }

    private Path getLatestFileFromJars(Path containerDir) throws IOException
    {
        JarVisitor visitor = new JarVisitor();

        Files.walkFileTree(containerDir, EnumSet.noneOf(FileVisitOption.class), 2, visitor);

        return visitor.getLatestFile();
    }

    private Version getLatestFromFile(Path containerDir) throws IOException
    {
        Path latestFile = containerDir.resolve(LATEST_FILE);

        if (Files.notExists(latestFile))
        {
            return null;
        }

        String versionString = new String(Files.readAllBytes(latestFile));
        try
        {
            if (StringUtils.isNotBlank(versionString))
            {
                return new GenericVersionScheme().parseVersion(versionString);
            }
        }
        catch (InvalidVersionSpecificationException e)
        {
            //ignore
        }

        return null;
    }

    private class JarVisitor extends SimpleFileVisitor<Path>
    {
        private Version latestVersion;
        private Path latestJar;

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
        {
            if (attrs.isRegularFile())
            {
                String filename = file.getFileName().toString();

                if (filename.startsWith(CONTAINER_JAR_PREFIX) && filename.endsWith(CONTAINER_JAR_SUFFIX))
                {
                    String jarVersionString = StringUtils.substringBetween(filename, CONTAINER_JAR_PREFIX, CONTAINER_JAR_SUFFIX);
                    try
                    {
                        Version jarVersion = new GenericVersionScheme().parseVersion(jarVersionString);
                        if (null == latestVersion || jarVersion.compareTo(latestVersion) > 0)
                        {
                            latestVersion = jarVersion;
                            latestJar = file;
                        }
                    }
                    catch (InvalidVersionSpecificationException e)
                    {
                        //ignore
                    }

                }
            }

            return FileVisitResult.CONTINUE;
        }

        public Version getLatestVersion()
        {
            return latestVersion;
        }

        public Path getLatestFile()
        {
            return latestJar;
        }

    }
}
