package com.atlassian.ap3.api.kit;

import java.nio.file.Path;

/**
 * @since version
 */
public class ProjectPackage
{
    private final Path projectRoot;
    private final Path resourcesDir;
    private final String pluginKey;
    private final Path jarFile;

    public ProjectPackage(Path projectRoot, Path resourcesDir, String pluginKey, Path jarFile)
    {
        this.projectRoot = projectRoot;
        this.resourcesDir = resourcesDir;
        this.pluginKey = pluginKey;
        this.jarFile = jarFile;
    }

    public Path getProjectRoot()
    {
        return projectRoot;
    }

    public Path getResourcesDir()
    {
        return resourcesDir;
    }

    public String getPluginKey()
    {
        return pluginKey;
    }

    public Path getJarFile()
    {
        return jarFile;
    }
}
