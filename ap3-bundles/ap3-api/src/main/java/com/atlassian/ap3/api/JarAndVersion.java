package com.atlassian.ap3.api;


import java.nio.file.Path;

public class JarAndVersion
{
    Path jar;
    String version;

    public JarAndVersion(Path jar, String version)
    {
        this.jar = jar;
        this.version = version;
    }

    public Path getJar()
    {
        return jar;
    }

    public String getVersion()
    {
        return version;
    }
}
