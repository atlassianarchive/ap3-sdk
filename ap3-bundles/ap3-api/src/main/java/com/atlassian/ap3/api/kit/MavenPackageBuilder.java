package com.atlassian.ap3.api.kit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.maven.MavenPomHelper;
import com.atlassian.ap3.api.util.JVMArgsUtil;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.building.*;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

@Named
@Singleton
public class MavenPackageBuilder
{
    private final MavenPomHelper pomHelper;
    
    @Inject
    public MavenPackageBuilder(MavenPomHelper pomHelper)
    {
        this.pomHelper = pomHelper;
    }

    public Path build(Path projectRoot, String mvnCommand, List<String> jvmArgs) throws IOException
    {
        checkState(StringUtils.isNotBlank(mvnCommand), "maven command is blank!");
        checkState((null != projectRoot && Files.exists(projectRoot)), "maven command is blank!");
        
        Path pomXml = projectRoot.resolve("pom.xml");
        if(Files.notExists(pomXml))
        {
            throw new IOException("Could not find pom in: " + pomXml.toAbsolutePath().toString());    
        }
        Model pom = pomHelper.readPom(pomXml);
        
        String artifactId = pom.getArtifactId();
        String version = pom.getVersion();
        
        String artifactName = artifactId + "-" + version;
        
        String finalName = pom.getBuild().getFinalName();
        if(StringUtils.isNotBlank(finalName))
        {
            artifactName = finalName;
        }
        
        ImmutableList.Builder<String> commandsBuilder = ImmutableList.builder();
        commandsBuilder.addAll(newArrayList(StringUtils.split(mvnCommand,' ')));

        List<String> defaultArgs = ImmutableList.<String>builder().add("-Xmx512m")
                                                .add("-XX:MaxPermSize=256m")
                                                .build();

        List<String> xArgs = defaultArgs;
        
        if (null != jvmArgs)
        {
            commandsBuilder.addAll(JVMArgsUtil.mergeToListNoXArgs(defaultArgs, jvmArgs));
            xArgs = JVMArgsUtil.mergeXArgsToList(defaultArgs,jvmArgs);
        }

        int exitCode = 0;
        try
        {
            String mvnOptString = System.getenv("MAVEN_OPTS");
            List<String> mvnOpts = xArgs;
            
            if(StringUtils.isNotBlank(mvnOptString))
            {
                mvnOpts = JVMArgsUtil.mergeXArgsToList(xArgs,newArrayList(StringUtils.split(mvnOptString,' ')));
            }
            
            List<String> commandLine = commandsBuilder.build();
            System.out.println("executing: " + StringUtils.join(commandLine, " "));
            
            ProcessBuilder processBuilder = new ProcessBuilder(commandLine);

            // execute the process in the specified buildRoot
            processBuilder.directory(projectRoot.toAbsolutePath().toFile());
            processBuilder.redirectErrorStream(true);
            processBuilder.environment().put("MAVEN_OPTS",StringUtils.join(mvnOpts,' '));

            final Process process = processBuilder.start();

            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        IOUtils.copy(process.getInputStream(), System.out);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }).start();

            exitCode = process.waitFor();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            throw new IOException(e);
        }
        
        if(exitCode > 0)
        {
            return null;    
        }
        
        Path targetDir = projectRoot.resolve("target");
        Path plugin = null;
        
        if(Files.exists(targetDir))
        {
            Path jar = targetDir.resolve(artifactName + ".jar");
            
            plugin = jar;
        }

        if(null == plugin || Files.notExists(plugin))
        {
            if(null == plugin)
            {
                throw new IOException("Could not find artifact: " + projectRoot.toAbsolutePath().toString() + File.separator + "target" + File.separator + artifactName + ".jar");
            }
            else
            {
                throw new IOException("Could not find artifact: " + plugin.toAbsolutePath().toString());
            }
        }
        
        return plugin;
    }

    
}
