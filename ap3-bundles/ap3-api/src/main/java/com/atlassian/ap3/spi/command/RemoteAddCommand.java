package com.atlassian.ap3.spi.command;

import java.io.IOException;

import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.spi.command.AbstractRemoteCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "add")
public class RemoteAddCommand extends AbstractRemoteCommand
{
    @Option(name = {"-i", "--id"}, title = "ID", description = "The id of the remote host entry")
    private String id;

    @Option(name = {"-h", "--host"}, title = "Host URL", description = "The url of the remote host")
    private String url;

    @Option(name = {"-un", "--username"}, title = "Username", description = "Your username on the remote host")
    private String username;

    @Option(name = {"-p", "--password"}, title = "Password", description = "Your password on the remote host")
    private String password;

    @Override
    public void run() throws Ap3Exception
    {

        if (StringUtils.isBlank(id))
        {
            id = promptForNewId();
        }
        
        if (StringUtils.isBlank(url))
        {
            url = prompter.prompt("Enter the url to the remote host","https://remoteapps.jira.com");
        }

        if (StringUtils.isBlank(username))
        {
            username = prompter.promptNotBlank("Enter your username for the remote host");
        }

        if (StringUtils.isBlank(password))
        {
            password = prompter.promptForPassword("Enter your password for the remote host");
        }
        
        id = checkForUpdate(id);

        RemoteHostConfigurationEntry entry = new RemoteHostConfigurationEntry();
        entry.setId(id);
        entry.setUrl(url);
        entry.setUsername(username);
        entry.setPassword(password);

        try
        {
            saveRemote(entry);
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error saving remote host entry", e);
        }
        
        prompter.showInfo("New host entry '" + id + "' saved!");

    }

    private String checkForUpdate(String id) throws PrompterException
    {
        String newId = id;
        if(null != getRemoteById(id))
        {
            prompter.showWarning("The id '" + id +"' already exists.");
            boolean update = prompter.promptForBoolean("Would you like to overwrite the entry?");
            if(!update)
            {
                newId = checkForUpdate(promptForNewId());
            }
        }
        
        return newId;
    }

    private String promptForNewId() throws PrompterException
    {
        return prompter.prompt("Enter an id for the remote host entry","rjc");
    }
}
