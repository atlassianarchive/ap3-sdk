package com.atlassian.ap3.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;

/**
 * @since version
 */
public class JVMArgsUtil
{
    public static String mergeToString(List<String> defaultArgs, List<String> userArgs)
    {
              
        return StringUtils.join(mergeToList(defaultArgs, userArgs),' ');
    }

    public static List<String> mergeToList(List<String> defaultArgs, List<String> userArgs)
    {
        List<String> mergedArgs = new ArrayList<>();

        Map<String,String> mergedMap = new HashMap<>();
        mergedMap.putAll(getArgsAsMap(defaultArgs));

        mergedMap.putAll(getArgsAsMap(userArgs));

        for(Map.Entry<String,String> entry : mergedMap.entrySet())
        {
            String key = entry.getKey();
            String val = entry.getValue();

            if(key.startsWith("-XX:+") || key.startsWith("-XX-"))
            {
                mergedArgs.add(key);
            }
            else if(key.startsWith("-Xmx") || key.startsWith("-Xms") || key.startsWith("-Xrunjdwp:"))
            {
                mergedArgs.add(key + val);
            }
            else if(key.startsWith("-XX:") || key.startsWith("-D"))
            {
                mergedArgs.add(key + val);
            }
            else if(key.startsWith("-X"))
            {
                mergedArgs.add(key);
            }
        }

        return mergedArgs;
    }

    public static List<String> mergeXArgsToList(List<String> defaultArgs, List<String> userArgs)
    {
        List<String> mergedArgs = new ArrayList<>();

        Map<String,String> mergedMap = new HashMap<>();
        mergedMap.putAll(getArgsAsMap(defaultArgs));

        mergedMap.putAll(getArgsAsMap(userArgs));

        for(Map.Entry<String,String> entry : mergedMap.entrySet())
        {
            String key = entry.getKey();
            String val = entry.getValue();

            if(key.startsWith("-XX:+") || key.startsWith("-XX-"))
            {
                mergedArgs.add(key);
            }
            else if(key.startsWith("-Xmx") || key.startsWith("-Xms") || key.startsWith("-Xrunjdwp:"))
            {
                mergedArgs.add(key + val);
            }
            else if(key.startsWith("-XX:"))
            {
                mergedArgs.add(key + val);
            }
            else if(key.startsWith("-X"))
            {
                mergedArgs.add(key);
            }
        }

        return mergedArgs;
    }

    public static List<String> mergeToListNoXArgs(List<String> defaultArgs, List<String> userArgs)
    {
        List<String> mergedArgs = new ArrayList<>();

        Map<String,String> mergedMap = new HashMap<>();
        mergedMap.putAll(getArgsAsMap(defaultArgs));

        mergedMap.putAll(getArgsAsMap(userArgs));

        for(Map.Entry<String,String> entry : mergedMap.entrySet())
        {
            String key = entry.getKey();
            String val = entry.getValue();

            if(key.startsWith("-D"))
            {
                mergedArgs.add(key + val);
            }
        }

        return mergedArgs;
    }
    
    private static Map<String,String> getArgsAsMap(List<String> args)
    {
        Map<String,String> argMap = new HashMap<>();
        for(String arg : args)
        {
            if(arg.startsWith("-XX:+") || arg.startsWith("-XX:-"))
            {
                argMap.put(arg, "");
            }
            else if(arg.startsWith("-XX:"))
            {
                String[] xnvp = StringUtils.split(arg,'=');
                if(xnvp.length > 1)
                {
                    argMap.put(xnvp[0] + "=", xnvp[1]);
                }
                else
                {
                    argMap.put(xnvp[0], "");
                }
            }
            else if(arg.startsWith("-Xms"))
            {
                argMap.put("-Xms", StringUtils.substringAfter(arg, "-Xms"));
            }
            else if(arg.startsWith("-Xmx"))
            {
                argMap.put("-Xmx", StringUtils.substringAfter(arg, "-Xmx"));
            }
            else if(arg.startsWith("-Xrunjdwp:"))
            {
                argMap.put("-Xrunjdwp:", StringUtils.substringAfter(arg, "-Xrunjdwp:"));
            }
            else if(arg.startsWith("-X"))
            {
                argMap.put(arg, "");
            }
            else if(arg.startsWith("-D"))
            {
                String[] nvp = StringUtils.split(arg,'=');
                if(nvp.length > 1)
                {
                    argMap.put(nvp[0] + "=", nvp[1]);
                }
                else
                {
                    argMap.put(nvp[0], "");
                }
            }
        }

        return argMap;
    }

}
