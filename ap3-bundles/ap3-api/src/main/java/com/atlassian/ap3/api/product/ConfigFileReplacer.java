package com.atlassian.ap3.api.product;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.util.NIOFileUtils;

@Named
@Singleton
public class ConfigFileReplacer
{
    public void replace(List<Path> files, List<ConfigFileReplacement> replacements, boolean inverted) throws IOException
    {
        for (Path file : files)
        {
            replace(file, replacements, inverted);
        }
    }

    /**
     * @param cfgFile      the file
     * @param replacements the list of keys to replace with values
     * @param inverted     if you want to swap values with keys. Be aware that the list is processed in order,
     *                     so that if 2 keys have the same value, the first key will be chosen. The Replacement records with
     *                     reversible set to false will not be reversed. Default: false.
     */
    public void replace(Path cfgFile, List<ConfigFileReplacement> replacements, boolean inverted) throws IOException
    {
        if (Files.notExists(cfgFile))
        {
            return;
        }
        String config = NIOFileUtils.readFileToString(cfgFile);
        if (!inverted)
        {
            for (ConfigFileReplacement replacement : replacements)
            {
                if (replacement.applyWhenUnzipping())
                {
                    config = config.replace(replacement.getKey(), replacement.getValue());
                }
            }
        }
        else
        {
            for (ConfigFileReplacement replacement : replacements)
            {
                if (replacement.applyWhenZipping())
                {
                    config = config.replace(replacement.getValue(), replacement.getKey());
                }
            }
        }
        NIOFileUtils.writeStringToFile(cfgFile, config);
    }

    public void replaceAll(Path cfgFile, String pattern, String replacement) throws IOException
    {
        if (Files.notExists(cfgFile))
        {
            return;
        }
        String config = NIOFileUtils.readFileToString(cfgFile);
        config = config.replaceAll(pattern, replacement); // obeys regex
        NIOFileUtils.writeStringToFile(cfgFile, config);
    }

}
