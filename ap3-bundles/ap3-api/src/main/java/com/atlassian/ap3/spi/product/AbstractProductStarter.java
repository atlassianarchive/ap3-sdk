package com.atlassian.ap3.spi.product;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.ap3.api.cargo.CargoRunner;
import com.atlassian.ap3.api.maven.MavenArtifactDownloader;
import com.atlassian.ap3.api.product.*;
import com.atlassian.ap3.api.util.JVMArgsUtil;
import com.atlassian.ap3.api.util.NIOFileUtils;
import com.atlassian.ap3.api.util.ZipFileSystemUtil;
import com.atlassian.ap3.home.HomeLocator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.aether.resolution.ArtifactResult;
import org.osgi.framework.BundleContext;

/**
 * @since version
 */
public abstract class AbstractProductStarter implements ProductStarter
{
    public static final ProductArtifact DEFAULT_CONTAINER = new ProductArtifact("org.apache.tomcat", "apache-tomcat", "6.0.32");
    @Inject
    private MavenArtifactDownloader artifactDownloader;

    @Inject
    protected HomeLocator homeLocator;
    
    @Inject
    protected ConfigFileReplacer configFileReplacer;
    
    @Inject
    private BundleContext bundleContext;
    
    @Inject
    CargoRunner cargoRunner;
    
    protected void startProduct(ProductArtifact productArtifact, ProductArtifact resourcesArtifact, ProductStartupConfig config) throws IOException
    {
        ProductContext ctx = null;
        ArtifactResult product = downloadProduct(new ProductArtifact(productArtifact.getGroupId(),productArtifact.getArtifactId(),config.getProductVersion()));

        if(null == config.getDataPath())
        {
            String dataVersion = config.getDataVersion();
            if(StringUtils.isBlank(dataVersion))
            {
                dataVersion = config.getProductVersion();
            }

            System.out.println("downloading " + getProductId() + " test resources...");
            ArtifactResult resourcesResult = artifactDownloader.downloadArtifact(resourcesArtifact.getGroupId(),resourcesArtifact.getArtifactId(),dataVersion,"zip");

            if(null == resourcesResult || null == resourcesResult.getArtifact())
            {
                throw new IOException("Unable to find " + getProductId() + " resources artifact");
            }
            ctx = createProductContext(product.getArtifact().getFile().toPath(),resourcesResult.getArtifact().getFile().toPath(),product.getArtifact().getVersion(),config);
        }
        else 
        {
            ctx = createProductContext(product.getArtifact().getFile().toPath(),config.getDataPath(),product.getArtifact().getVersion(),config);
        }
        
        startProduct(ctx);
        
    }

    private ProductContext createProductContext(Path productWar, Path resourceZip, String version, ProductStartupConfig config) throws IOException
    {
        String contextPath = getContextPath();
        if (!contextPath.startsWith("/") && StringUtils.isNotBlank(contextPath))
        {
            contextPath = "/" + contextPath;
        }

        return new ProductContext(productWar
                ,resourceZip
                ,getProductHomeDirectory(config.getPort(),getProductId())
                ,getProductAppDirectory(getProductId(),version)
                , config.getPort()
                ,version
                ,getProductId()
                ,contextPath
                ,config.doCleanHome()
                ,config.doDebug()
                ,config.doDebugSuspend()
                ,config.getDebugPort()
                ,config.getJvmArgs());
    }

    private void startProduct(ProductContext ctx) throws IOException
    {
        extractAndProcessHomeDirectory(ctx);
        Path warFile = Files.copy(ctx.getProductWar(),ctx.getAppDir().resolve(ctx.getProductWar().getFileName()),StandardCopyOption.REPLACE_EXISTING);
        
        startWebapp(warFile,ctx);
    }

    private void startWebapp(Path warFile, ProductContext ctx) throws IOException
    {
        Path webConfigDir = ctx.getAppDir().resolve("cargo-" + getProductId() + "-home");
        Files.createDirectories(webConfigDir);

        final int rmiPort = pickFreePort(0);
        int actualHttpPort = pickFreePort(ctx.getPort());
        String protocol = "http";

        String contextPath = getContextPath();
        if (!contextPath.startsWith("/") && StringUtils.isNotBlank(contextPath))
        {
            contextPath = "/" + contextPath;
        }
        
        String baseUrl = protocol + "://localhost:" + actualHttpPort + contextPath;

        Map<String,String> sysProps = getCargoSystemProperties(ctx);
        
        sysProps.put("baseurl",baseUrl);

        Map<String,String> props = getCargoSystemProperties(ctx);
        props.put("cargo.servlet.port",String.valueOf(actualHttpPort));
        props.put("cargo.protocol",protocol);
        props.put("cargo.rmi.port",String.valueOf(rmiPort));
        
        List<String> jvmArgs = Lists.newArrayList("-Xmx512m","-XX:MaxPermSize=256m");
        if(ctx.doDebug())
        {
            String suspend = "n";
            if(ctx.doDebugSuspend())
            {
                suspend = "y";
            }
            
            jvmArgs.add("-Xdebug");
            jvmArgs.add("-Xrunjdwp:transport=dt_socket,server=y,suspend=" + suspend + ",address=" + ctx.getDebugPort());
        }
        
        String mergedArgs = JVMArgsUtil.mergeToString(jvmArgs,ctx.getJvmArgs());
        
        props.put("cargo.jvmargs",mergedArgs);
        
        System.out.println("Starting " + ctx.getProductId() + " version " + ctx.getVersion());
        cargoRunner.runContainerAndWait(ctx,baseUrl,webConfigDir,warFile,sysProps,props);
        
    }

    private Map<String, String> getCargoSystemProperties(ProductContext ctx)
    {
        Map<String,String> map = Maps.newHashMap();
        map.put("atlassian.dev.mode", "true");
        map.put("java.awt.headless", "true");
        
        map.putAll(getSystemProperties(ctx));
        
        
        return map;
    }

    private Path getProductAppDirectory(String productId, String version) throws IOException
    {
        Path appDir = homeLocator.getContainerDirectory().resolve(productId + "-" + version);
        Files.createDirectories(appDir);
        
        return appDir;
    }

    private void extractAndProcessHomeDirectory(ProductContext ctx) throws IOException
    {
        final Path productHome = ctx.getHomeDir();
        
        if(ctx.doCleanHome() && !NIOFileUtils.directoryIsEmpty(productHome))
        {
            NIOFileUtils.cleanDirectory(productHome);
        }

        if(NIOFileUtils.directoryIsEmpty(productHome))
        {
            extractHomeData(ctx.getResourcesZip(),productHome);
            processHomeDirectory(ctx);
        }
    }

    private void extractHomeData(Path homeData, final Path productHome) throws IOException
    {
        try(FileSystem homeZip = ZipFileSystemUtil.openZipFileSystem(homeData))
        {
            Files.walkFileTree(homeZip.getPath("/"),new SimpleFileVisitor<Path>(){
                
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
                {
                    //we expect /zipfilename & /zipfilename/product-home which we need to ignore
                    if(dir.getNameCount() > 2)
                    {
                        Path subPath = dir.subpath(2,dir.getNameCount());
                        Path newDir = Paths.get(productHome.toString(),subPath.toString());
                        Files.createDirectories(newDir);
                    }
                    
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {
                    int nameCount = file.getNameCount();
                    Path subPath = file.subpath(2,nameCount);
                    Path dest = Paths.get(productHome.toString(), subPath.toString());
                    if(Files.notExists(dest))
                    {
                        Files.copy(file,dest,StandardCopyOption.REPLACE_EXISTING);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    private ArtifactResult downloadProduct(ProductArtifact productArtifact) throws IOException
    {
        System.out.println("downloading " + getProductId() + "...");
        ArtifactResult productResult = artifactDownloader.downloadArtifact(productArtifact.getGroupId(),productArtifact.getArtifactId(),productArtifact.getVersionOrRange(),"war");

        if(null == productResult || null == productResult.getArtifact())
        {
            throw new IOException("Unable to find " + getProductId() + " webapp artifact");
        }

        return productResult;
    }
    
    protected void processHomeDirectory(ProductContext ctx) throws IOException
    {
        configFileReplacer.replace(getConfigFiles(ctx.getHomeDir()),getConfigReplacements(ctx),false);
    }

    protected Path getProductHomeDirectory(int port, String productId) throws IOException
    {
        Path homeDir = homeLocator.getContainerDirectory().resolve(productId + "-home-" + port);
        if(Files.notExists(homeDir))
        {
            Files.createDirectories(homeDir);
        }
        
        return homeDir;
    }

    public List<Path> getConfigFiles(Path productHome)
    {
        return Lists.newArrayList();
    }

    public List<ConfigFileReplacement> getConfigReplacements(ProductContext ctx)
    {
        // Standard replacements:
        List<ConfigFileReplacement> replacements = Lists.newArrayList();
        String homeDirectory = ctx.getHomeDir().toAbsolutePath().toString();

        replacements.add(new ConfigFileReplacement("%PRODUCT_HOME_DIR%", homeDirectory));
        replacements.add(new ConfigFileReplacement("%LOCAL_HOST_NAME%", "localhost"));
        
        return replacements;
    }

    protected abstract String getProductId();
    protected abstract String getContextPath();
    protected abstract Map<String,String> getSystemProperties(ProductContext ctx);

    int pickFreePort(final int requestedPort)
    {
        ServerSocket socket = null;
        try
        {
            socket = new ServerSocket(requestedPort);
            return requestedPort > 0 ? requestedPort : socket.getLocalPort();
        }
        catch (final IOException e)
        {
            // happens if the requested port is taken, so we need to pick a new one
            ServerSocket zeroSocket = null;
            try
            {
                zeroSocket = new ServerSocket(0);
                return zeroSocket.getLocalPort();
            }
            catch (final IOException ex)
            {
                throw new RuntimeException("Error opening socket", ex);
            }
            finally
            {
                closeSocket(zeroSocket);
            }
        }
        finally
        {
            closeSocket(socket);
        }
    }

    private void closeSocket(ServerSocket socket)
    {
        if (socket != null)
        {
            try
            {
                socket.close();
            }
            catch (final IOException e)
            {
                throw new RuntimeException("Error closing socket", e);
            }
        }
    }
    
}
