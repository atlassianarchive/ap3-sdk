package com.atlassian.ap3.cli;

import javax.inject.Inject;

import com.atlassian.ap3.api.GlobalOptions;
import com.atlassian.ap3.spi.command.Ap3Command;

import org.eclipse.sisu.Nullable;

import io.airlift.command.Help;

public class Ap3Help extends Help implements Ap3Command
{
    @Inject
    @Nullable
    protected GlobalOptions globalOptions = new GlobalOptions();

    @Override
    public void setArgs(String[] args)
    {
        //do nothing
    }
}
