package com.atlassian.ap3.api.product;

import java.nio.file.Path;
import java.util.List;

/**
 * @since version
 */
public class ProductContext
{
    private final Path productWar;
    private final Path resourcesZip;
    private final Path homeDir;
    private final Path appDir;
    private final int port;
    private final String version;
    private final String productId;
    private final String contextPath;
    private final boolean cleanHome;
    private final boolean debug;
    private final boolean debugSuspend;
    private final int debugPort;
    private final List<String> jvmArgs;

    public ProductContext(Path productWar, Path resourcesZip, Path homeDir, Path appDir, int port, String version, String productId, String contextPath, boolean cleanHome, boolean debug, boolean debugSuspend, int debugPort, List<String> jvmArgs)
    {
        this.productWar = productWar;
        this.resourcesZip = resourcesZip;
        this.homeDir = homeDir;
        this.appDir = appDir;
        this.port = port;
        this.version = version;
        this.productId = productId;
        this.contextPath = contextPath;
        this.cleanHome = cleanHome;
        this.debug = debug;
        this.debugSuspend = debugSuspend;
        this.debugPort = debugPort;
        this.jvmArgs = jvmArgs;
    }

    public Path getProductWar()
    {
        return productWar;
    }

    public Path getResourcesZip()
    {
        return resourcesZip;
    }

    public Path getHomeDir()
    {
        return homeDir;
    }

    public Path getAppDir()
    {
        return appDir;
    }

    public int getPort()
    {
        return port;
    }

    public String getVersion()
    {
        return version;
    }

    public String getContextPath()
    {
        return contextPath;
    }

    public String getProductId()
    {
        return productId;
    }

    public boolean doCleanHome()
    {
        return cleanHome;
    }

    public boolean doDebug()
    {
        return debug;
    }

    public boolean doDebugSuspend()
    {
        return debugSuspend;
    }

    public int getDebugPort()
    {
        return debugPort;
    }

    public List<String> getJvmArgs()
    {
        return jvmArgs;
    }
}
