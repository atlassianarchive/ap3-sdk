package com.atlassian.ap3.api.config;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("remotes")
public class RemotesConfigurationEntry
{
    private List<RemoteHostConfigurationEntry> remotes;

    public List<RemoteHostConfigurationEntry> getRemotes()
    {
        if(null == this.remotes)
        {
            this.remotes = new ArrayList<>();
        }
        
        return remotes;
    }

    public void setRemotes(List<RemoteHostConfigurationEntry> remotes)
    {
        this.remotes = remotes;
    }
}
