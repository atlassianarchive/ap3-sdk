package com.atlassian.ap3.api.template;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

/**
 * @since version
 */
public class TemplateVariable
{
    private String name;
    private String prompt;
    private String defaultValue;
    private String staticValue;
    
    private Set<String> choices;

    public String getName()
    {
        return name;
    }

    public String getPrompt()
    {
        return prompt;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public String getStaticValue()
    {
        return staticValue;
    }

    public Set<String> getChoices()
    {
        if(null == choices)
        {
            choices = ImmutableSet.<String> of();
        }
        return choices;
    }
}
