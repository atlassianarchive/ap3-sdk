package com.atlassian.ap3.api.template;


import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import com.atlassian.ap3.api.oauth.OAuthKeyPair;
import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.inject.ImplementedBy;

@ImplementedBy(ProjectGeneratorImpl.class)
public interface ProjectGenerator
{
    Path generateProject(TemplateInfo templateInfo, Map<String, Object> context, PluginIdentifier pluginId, KitDescriptor kit, OAuthKeyPair keyPair) throws IOException;
}
