package com.atlassian.ap3.spi.kit;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import com.atlassian.ap3.api.kit.ProjectPackage;

/**
 * @since version
 */
public interface ProjectPackager
{
    ProjectPackage packageProject(Path projectRoot, Path containerJar, String containerVersion, List<String> jvmArgs) throws IOException;
}
