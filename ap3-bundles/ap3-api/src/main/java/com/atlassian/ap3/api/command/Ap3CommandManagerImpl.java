package com.atlassian.ap3.api.command;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.spi.command.Ap3Command;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import io.airlift.command.Command;

@Named("commandManager")
public final class Ap3CommandManagerImpl implements Ap3CommandManager
{
    private Map<String, Ap3Command> commands;

    @Inject
    public Ap3CommandManagerImpl(Map<String, Ap3Command> commands)
    {
        this.commands = commands;
    }
    
    @Override
    public Set<Ap3Command> getAllCommands()
    {
        return ImmutableSet.copyOf(commands.values());
    }

    @Override
    public <T> Set<T> getCommands(final Class<? extends Ap3Command> clazz)
    {
        Set<T> cmdSet = new HashSet<>();
        
        for(Ap3Command command : commands.values())
        {
            if(clazz.isAssignableFrom(command.getClass()))
            {
                cmdSet.add((T) command);
            }
        }
        return ImmutableSet.copyOf(cmdSet);
    }

    @Override
    public Iterable<Class<? extends Ap3Command>> getCommandClasses()
    {
        return ImmutableSet.copyOf(Iterables.transform(commands.values(), new Function<Ap3Command, Class<? extends Ap3Command>>()
        {
            @Override
            public Class<? extends Ap3Command> apply(Ap3Command ap3Command)
            {
                return ap3Command.getClass();
            }
        }));
    }

    @Override
    public Ap3Command getCommandByName(String name)
    {
        Ap3Command found = null;

        for (Ap3Command command : commands.values())
        {
            Command commandAnno = command.getClass().getAnnotation(Command.class);

            if (null != commandAnno)
            {
                String commandName = commandAnno.name();

                if (commandName.equals(name))
                {
                    found = command;
                    break;
                }
            }
        }

        return found;
    }

    @Override
    public Ap3Command getCommandByType(Class<?> type)
    {
        Ap3Command found = null;

        for (Ap3Command command : commands.values())
        {

            if (type.equals(command.getClass()))
            {
                found = command;
                break;
            }
        }

        return found;
    }
}
