package com.atlassian.ap3.api.cargo;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.BundleDelegatingClassloader;
import com.atlassian.ap3.api.Ap3ApiBundleProvider;
import com.atlassian.ap3.api.product.ProductContext;
import com.atlassian.ap3.api.util.LaunchBrowser;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.cargo.container.ContainerType;
import org.codehaus.cargo.container.InstalledLocalContainer;
import org.codehaus.cargo.container.configuration.ConfigurationType;
import org.codehaus.cargo.container.configuration.LocalConfiguration;
import org.codehaus.cargo.container.deployable.WAR;
import org.codehaus.cargo.container.installer.ZipURLInstaller;
import org.codehaus.cargo.container.internal.util.ResourceUtils;
import org.codehaus.cargo.container.spi.util.ContainerUtils;
import org.codehaus.cargo.generic.DefaultContainerFactory;
import org.codehaus.cargo.generic.configuration.DefaultConfigurationFactory;
import org.codehaus.cargo.util.log.Logger;
import org.codehaus.cargo.container.State;

@Named
@Singleton
public class CargoRunner
{
    private final Ap3ApiBundleProvider ap3ApiBundleProvider;
    
    @Inject
    public CargoRunner(Ap3ApiBundleProvider ap3ApiBundleProvider)
    {
        this.ap3ApiBundleProvider = ap3ApiBundleProvider;
    }

    public void runContainerAndWait(ProductContext ctx, String baseUrl, Path configDir, Path warFile, Map<String, String> sysProps, Map<String, String> props) throws IOException
    {
        ZipURLInstaller installer = new ZipURLInstaller(new URL("http://archive.apache.org/dist/tomcat/tomcat-6/v6.0.32/bin/apache-tomcat-6.0.32.zip"));
        installer.setExtractDir(ctx.getAppDir().toAbsolutePath().toString());
        installer.install();

        BundleDelegatingClassloader bcl = new BundleDelegatingClassloader(ap3ApiBundleProvider.getApiBundle());

        ResourceUtils.setResourceLoader(bcl);
        DefaultConfigurationFactory configurationFactory = new DefaultConfigurationFactory(bcl);
        DefaultContainerFactory containerFactory = new DefaultContainerFactory(bcl);

        LocalConfiguration cargoConfig = (LocalConfiguration) configurationFactory.createConfiguration("tomcat6x", ContainerType.INSTALLED, ConfigurationType.STANDALONE,configDir.toAbsolutePath().toString());

        final InstalledLocalContainer container = (InstalledLocalContainer) containerFactory.createContainer("tomcat6x", ContainerType.INSTALLED, cargoConfig);
        container.setHome(installer.getHome());
        container.setSystemProperties(sysProps);


        WAR deployable = new WAR(warFile.toAbsolutePath().toString());
        deployable.setContext(ctx.getContextPath());

        cargoConfig.addDeployable(deployable);
        cargoConfig.getProperties().putAll(props);

        final Logger log = new Ap3CargoLogger("tomcat6x:" + ctx.getProductId());

        container.setLogger(log);

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    if (container != null && (org.codehaus.cargo.container.State.STARTED == container.getState() || org.codehaus.cargo.container.State.STARTING == container.getState()))
                    {
                        log.info("Stopping container...","");
                        container.stop();
                    }
                }
                catch (Exception e)
                {
                    log.warn("Failed stopping the container", "");
                }
            }
        });

        long globalStartTime = System.nanoTime();
        container.start();
        long globalDurationSeconds = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - globalStartTime);
        
        log.info("Container startup duration was " + globalDurationSeconds + " seconds","");
        log.info(StringUtils.capitalize(ctx.getProductId()) + " viewable at " + baseUrl,"");
        log.info("Type Ctrl-C to exit","tomcat6x:" + ctx.getProductId());

        LaunchBrowser.launch(baseUrl);
        
        ContainerUtils.waitTillContainerIsStopped(container);
    }
}
