package com.atlassian.ap3.api.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.regex.Matcher;

import com.google.common.base.CaseFormat;

import org.apache.commons.lang3.StringUtils;

/**
 * @since version
 */
public class NamingUtil
{
    public static String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";

    private static final SecureRandom random = new SecureRandom();
    
    public static String safeFilename(String name)
    {
        return name.replaceAll(CLEAN_FILENAME_PATTERN, "-").toLowerCase();
    }
    
    public static Path tempFilename(String prefix, String suffix)
    {
        long n = random.nextLong();
        n = (n == Long.MIN_VALUE) ? 0 : Math.abs(n);
        return Paths.get(System.getProperty("java.io.tmpdir")).resolve(prefix + Long.toString(n) + suffix);
    }

    public static String camelCaseOrSpaceToDashed(String s)
    {
        String dashed = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN,s);
        String trimmed = StringUtils.replace(StringUtils.replace(StringUtils.replace(dashed," -","-"),"- ","-")," ","-");
        
        return trimmed.toLowerCase();
    }

    public static String camelCase(String s)
    {
        
        String spaced = s.replaceAll("[-]", " ");
        String[] parts = StringUtils.split(spaced, ' ');
        StringBuilder b = new StringBuilder();
        
        for(int i=0;i<parts.length;i++)
        {
            if(i < 1)
            {
                b.append(parts[i].toLowerCase());
            }
            else
            {
                b.append(StringUtils.capitalize(parts[i]));
            }

        }
        
        return b.toString();
    }

    public static String cappedCamelCase(String s)
    {

        String spaced = s.replaceAll("[-]", " ");
        String[] parts = StringUtils.split(spaced, ' ');
        StringBuilder b = new StringBuilder();

        for(int i=0;i<parts.length;i++)
        {
            b.append(StringUtils.capitalize(parts[i]));
        }

        return b.toString();
    }

    public static String fullyQualifiedName(String packageName, String className)
    {
        String fqName = "";
        String packagePrefix = "";
        if (StringUtils.isNotBlank(packageName))
        {
            packagePrefix = packageName.endsWith(".") ? packageName : packageName + ".";
        }

        if (StringUtils.isNotBlank(className))
        {
            fqName = packagePrefix + className;
        }

        return fqName;
    }
    
    public static String packageToPath(String packageName)
    {
        return packageName.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
    }

    public static boolean isValidClassName(String s)
    {
        if (s.length() < 1)
        {
            return false;
        }
        if (s.equals("package-info"))
        {
            return false;
        }
        int cp = s.codePointAt(0);
        if (!Character.isJavaIdentifierStart(cp))
        {
            return false;
        }
        for (int j = Character.charCount(cp); j < s.length(); j += Character.charCount(cp))
        {
            cp = s.codePointAt(j);
            if (!Character.isJavaIdentifierPart(cp))
            {
                return false;
            }
        }

        return true;
    }
    
    public static boolean isValidPackageName(String s)
    {
        int index;
        while ((index = s.indexOf('.')) != -1)
        {
            if (!isValidClassName(s.substring(0, index)))
            {
                return false;
            }
            s = s.substring(index + 1);
        }
        return isValidClassName(s);
    }
}
