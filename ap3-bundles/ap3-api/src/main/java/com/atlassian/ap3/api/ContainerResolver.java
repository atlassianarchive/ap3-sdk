package com.atlassian.ap3.api;

import java.io.IOException;
import java.nio.file.Path;

import com.google.inject.ImplementedBy;

@ImplementedBy(ContainerResolverImpl.class)
public interface ContainerResolver
{
    String getLatestContainerVersion() throws IOException;
    String getLatestContainerVersion(boolean forceUpdate) throws IOException;
    Path getLatestContainerJar() throws IOException;
    Path getLatestContainerJar(boolean forceUpdate) throws IOException;
    Path getContainerJar(String version,boolean forceUpdate) throws IOException;
}
