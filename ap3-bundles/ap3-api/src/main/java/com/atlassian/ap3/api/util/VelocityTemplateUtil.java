package com.atlassian.ap3.api.util;

import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;

/**
 * @since version
 */
public class VelocityTemplateUtil
{
    public static final String UTF8 = "UTF-8";
//
//    static
//    {
//        Velocity.setProperty(RuntimeConstants.INPUT_ENCODING, UTF8);
//        Velocity.setProperty(RuntimeConstants.PARSER_POOL_SIZE, 3);
//        Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "file, classpath");
//        Velocity.setProperty("classpath." + RuntimeConstants.RESOURCE_LOADER + ".class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//        Velocity.setProperty("file." + RuntimeConstants.RESOURCE_LOADER + ".class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
//        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.NullLogChute");
//        try
//        {
//            Velocity.init();
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    public static String parseTemplate(String templatePath, Map<String, Object> props, String templateDir) throws Exception
    {
        VelocityEngine ve = new VelocityEngine();

        ve.setProperty(RuntimeConstants.INPUT_ENCODING, UTF8);
        ve.setProperty(RuntimeConstants.PARSER_POOL_SIZE, 3);
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "file, classpath");
        ve.setProperty("classpath." + RuntimeConstants.RESOURCE_LOADER + ".class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.setProperty("file." + RuntimeConstants.RESOURCE_LOADER + ".class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.NullLogChute");
        ve.setProperty("file." + RuntimeConstants.RESOURCE_LOADER + ".path", templateDir);
        
        ve.init();
        
        VelocityContext ctx = new VelocityContext();

        for (Map.Entry<String, Object> entry: props.entrySet())
        {
            ctx.put(entry.getKey(), entry.getValue());
        }

        final StringWriter stringWriter = new StringWriter();
        Template template = ve.getTemplate(templatePath);

        template.merge(ctx, stringWriter);

        return stringWriter.toString();
    }
}
