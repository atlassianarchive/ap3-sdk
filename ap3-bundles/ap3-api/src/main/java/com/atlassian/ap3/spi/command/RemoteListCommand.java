package com.atlassian.ap3.spi.command;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.api.config.RemotesConfigurationEntry;
import com.atlassian.ap3.api.prompt.Prompter;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Group;

@Named
@Group(name = "remote", description = "Configures available remote hosts", defaultCommand = RemoteListCommand.class)
@Command(name = "list")
public class RemoteListCommand extends AbstractRemoteCommand
{
    @Override
    public void run() throws Ap3Exception
    {
        if(getRemotes().isEmpty())
        {
            prompter.showWarning("No remotes are setup.");
            prompter.showMessage("run: 'ap3 remote add' to add a new remote host.");
        }
        else 
        {
            prompter.showInfo("Current Remote Hosts:");
            for(RemoteHostConfigurationEntry entry : getRemotes())
            {
                StringBuilder sb = new StringBuilder();
                sb.append("---------------------------------------").append(System.lineSeparator())
                        .append("id: ").append(entry.getId()).append(System.lineSeparator())
                        .append("url: ").append(entry.getUrl()).append(System.lineSeparator())
                        .append("username: ").append(entry.getUsername()).append(System.lineSeparator())
                        .append("password: ").append(StringUtils.repeat('*',entry.getPassword().length())).append(System.lineSeparator())
                .append("---------------------------------------").append(System.lineSeparator());
                prompter.showMessage(sb.toString());
            }
        }
    }
}
