package com.atlassian.ap3.api.config;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

public final class Ap3Configuration
{
    private Map<String,Object> ap3Configuration;

    public Ap3Configuration()
    {
        ap3Configuration = new HashMap<>();
    }

    // this is here for testing
    public Ap3Configuration(Map<String,Object> ap3Configuration)
    {
        this.ap3Configuration = ap3Configuration;
    }

    public <T> T getConfigurationEntry(Class<T> clazz)
    {
        for(Map.Entry<String,Object> entry: ap3Configuration.entrySet())
        {
            if(entry.getValue().getClass().equals(clazz))
            {
                return (T) entry.getValue();
            }
        }
        
        return null;
    }
    
    public void updateConfigurationEntry(Object configEntry)
    {
        String foundKey = null;
        
        for(Map.Entry<String,Object> entry: ap3Configuration.entrySet())
        {
            if(entry.getValue().getClass().equals(configEntry.getClass()))
            {
                foundKey = entry.getKey();
                break;
            }
        }
        
        if(null == foundKey)
        {
            if(configEntry.getClass().isAnnotationPresent(ConfigurationEntry.class))
            {
                foundKey = configEntry.getClass().getAnnotation(ConfigurationEntry.class).value();
            }
            else
            {
                foundKey = configEntry.getClass().getSimpleName();
            }
        }
        
        ap3Configuration.put(foundKey,configEntry);
    }

    public void removeConfigurationEntry(Object configEntry)
    {
        String foundKey = null;

        for(Map.Entry<String,Object> entry: ap3Configuration.entrySet())
        {
            if(entry.getValue().getClass().equals(configEntry.getClass()))
            {
                foundKey = entry.getKey();
                break;
            }
        }

        if(null == foundKey)
        {
            if(configEntry.getClass().isAnnotationPresent(ConfigurationEntry.class))
            {
                foundKey = configEntry.getClass().getAnnotation(ConfigurationEntry.class).value();
            }
            else
            {
                foundKey = configEntry.getClass().getSimpleName();
            }
        }

        if(ap3Configuration.containsKey(foundKey))
        {
            ap3Configuration.remove(foundKey);
        }
    }
}
