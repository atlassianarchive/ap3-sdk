package com.atlassian.ap3.api;

import io.airlift.command.Option;

import static io.airlift.command.OptionType.GLOBAL;

public class GlobalOptions
{
    @Option(type = GLOBAL, name = {"-o", "--offline"}, description = "Offline mode")
    private boolean offline = false;

    @Option(type = GLOBAL, name = {"-u", "--update"}, description = "Force update of the AP3 SDK")
    private boolean update = false;

    @Option(type = GLOBAL, name = {"-ud", "--update-develop"}, hidden = true)
    private boolean updateDevelop = false;

    public boolean isOffline()
    {
        return offline;
    }

    public boolean doUpdate()
    {
        return update;
    }

    public boolean doUpdateDevelop()
    {
        return updateDevelop;
    }
}
