package com.atlassian.ap3.base.command;

import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.base.config.BitbucketConfigurationEntry;
import com.atlassian.ap3.spi.command.AbstractConfigCommand;

import com.google.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "bitbucket", description = "Configures Bitbucket credentials")
public class BitbucketConfigCommand extends AbstractConfigCommand<BitbucketConfigurationEntry>
{
    @Option(name = {"-bbu", "--bitbucket-username"}, title = "Bitbucket Username", description = "Your Bitbucket username")
    private String bitbucketUsername;

    @Override
    protected BitbucketConfigurationEntry getUpdatedConfigurationEntry() throws Ap3Exception
    {
        BitbucketConfigurationEntry config = getCurrentConfigurationEntry(getEntryType());

        if(null == config)
        {
            config = new BitbucketConfigurationEntry();
        }

        if(StringUtils.isBlank(bitbucketUsername))
        {
            try
            {
                String bbUser;
                if(StringUtils.isNotBlank(config.getUsername()))
                {
                    bbUser = prompter.i18nPromptNotBlank("ap3.config.bb.username.prompt",config.getUsername());
                }
                else
                {
                    bbUser = prompter.i18nPromptNotBlank("ap3.config.bb.username.prompt");
                }

                config.setUsername(bbUser);

            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("unable to prompt for bitbucket username",e);
            }

        }
        else
        {
            config.setUsername(bitbucketUsername);
        }
        
        return config;
    }

}
