package com.atlassian.ap3.base.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("browser")
public class BrowserConfigurationEntry
{
    private Boolean launch;


    public Boolean getLaunch()
    {
        return launch;
    }

    public void setLaunch(Boolean launch)
    {
        this.launch = launch;
    }
}
