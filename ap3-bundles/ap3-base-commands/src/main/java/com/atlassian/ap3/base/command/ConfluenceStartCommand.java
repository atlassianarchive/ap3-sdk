package com.atlassian.ap3.base.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.product.ProductStartupConfig;
import com.atlassian.ap3.base.product.ConfluenceProductStarter;
import com.atlassian.ap3.spi.command.AbstractProductStartCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;

@Named
@Command(name = "confluence", description = "Starts Confluence")
public class ConfluenceStartCommand extends AbstractProductStartCommand
{
    private final ConfluenceProductStarter productStarter;

    @Inject
    public ConfluenceStartCommand(ConfluenceProductStarter productStarter)
    {
        this.productStarter = productStarter;
    }

    @Override
    public void run() throws Ap3Exception
    {
        try
        {
            String pVersion = version;
            if(StringUtils.isBlank(pVersion) || "LATEST".equals(pVersion))
            {
                pVersion = "[5.0-beta3,)";
            }

            int connyPort = port;
            if(connyPort < 1)
            {
                connyPort = 1990;
            }

            ProductStartupConfig.Builder builder = ProductStartupConfig.builder(pVersion,connyPort);
            builder.setCleanHome(cleanHome)
                   .setDebug(debug)
                   .setDebugPort(debugPort)
                   .setDebugSuspend(debugSuspend)
                   .setJvmArgs(getExtraJvmArgs())
                   .setDataVersion(dataVersion);

            if(StringUtils.isNotBlank(dataPath))
            {
                Path homeZip = Paths.get(dataPath);
                if(Files.exists(homeZip))
                {
                    builder.setDataPath(homeZip);
                }
            }

            productStarter.startProduct(builder.build());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
