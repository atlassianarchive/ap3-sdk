package com.atlassian.ap3.base.product;


import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.api.kit.ProjectPackage;

public class P3StartupContext
{
    private Integer port;
    private ProjectPackage projectPackage;
    private String localAppUrl;
    private String mountBaseUrl;
    private String browserLaunchUrl;
    private Path containerJar;
    private String containerVersion;
    private boolean debug;
    private Integer debugPort;
    private boolean debugSuspend;
    private String verbosityFlag;
    private List<String> extraJvmArgs;
    private RemoteHostConfigurationEntry remoteHost;
    private boolean productionMode;
    private boolean launchBrowser;

    public P3StartupContext(Integer port, ProjectPackage projectPackage, Path containerJar, String containerVersion)
    {
        this.port = port;
        this.projectPackage = projectPackage;
        this.containerJar = containerJar;
        this.containerVersion = containerVersion;
        this.localAppUrl = "http://localhost:" + port + "/" + projectPackage.getPluginKey();
        this.mountBaseUrl = "http://localhost:" + port;
        this.browserLaunchUrl = localAppUrl;
        this.debug = false;
        this.debugSuspend = false;
        this.debugPort = 5004;
        this.verbosityFlag = "";
        this.extraJvmArgs = new ArrayList<>();
        this.remoteHost = null;
    }

    public Integer getPort()
    {
        return port;
    }
    
    public String getPluginKey()
    {
        return projectPackage.getPluginKey();
    }
    
    public Path getProjectRoot()
    {
        return projectPackage.getProjectRoot();
    }

    public Path getPluginJar()
    {
        return projectPackage.getJarFile();
    }

    public Path getProjectResources()
    {
        return projectPackage.getResourcesDir();
    }

    public Path getContainerJar()
    {
        return containerJar;
    }

    public String getContainerVersion()
    {
        return containerVersion;
    }

    public String getLocalAppUrl()
    {
        return localAppUrl;
    }

    public void setLocalAppUrl(String localAppUrl)
    {
        this.localAppUrl = localAppUrl;
    }

    public String getMountBaseUrl()
    {
        return mountBaseUrl;
    }

    public void setMountBaseUrl(String mountBaseUrl)
    {
        this.mountBaseUrl = mountBaseUrl;
    }

    public String getBrowserLaunchUrl()
    {
        return browserLaunchUrl;
    }

    public void setBrowserLaunchUrl(String browserLaunchUrl)
    {
        this.browserLaunchUrl = browserLaunchUrl;
    }

    public boolean isDebug()
    {
        return debug;
    }

    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }

    public Integer getDebugPort()
    {
        return debugPort;
    }

    public void setDebugPort(Integer debugPort)
    {
        this.debugPort = debugPort;
    }

    public boolean isDebugSuspend()
    {
        return debugSuspend;
    }

    public void setDebugSuspend(boolean debugSuspend)
    {
        this.debugSuspend = debugSuspend;
    }

    public String getVerbosityFlag()
    {
        return verbosityFlag;
    }

    public void setVerbosityFlag(String verbosityFlag)
    {
        this.verbosityFlag = verbosityFlag;
    }

    public List<String> getExtraJvmArgs()
    {
        return extraJvmArgs;
    }

    public void setExtraJvmArgs(List<String> extraJvmArgs)
    {
        this.extraJvmArgs = extraJvmArgs;
    }

    public void setRemoteHost(RemoteHostConfigurationEntry remoteHost)
    {
        this.remoteHost = remoteHost;
    }

    public RemoteHostConfigurationEntry getRemoteHost()
    {
        return remoteHost;
    }

    public void setProductionMode(boolean productionMode)
    {
        this.productionMode = productionMode;
    }

    public boolean isProductionMode()
    {
        return productionMode;
    }

    public void setLaunchBrowser(boolean launchBrowser)
    {
        this.launchBrowser = launchBrowser;
    }

    public boolean getLaunchBrowser()
    {
        return launchBrowser;
    }
}
