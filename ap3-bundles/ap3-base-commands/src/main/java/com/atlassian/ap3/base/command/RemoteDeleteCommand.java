package com.atlassian.ap3.base.command;

import java.io.IOException;

import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.spi.command.AbstractRemoteCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "delete")
public class RemoteDeleteCommand extends AbstractRemoteCommand
{
    @Option(name = {"-i", "--id"}, title = "ID", description = "The id of the remote host entry")
    private String id;
    
    @Override
    public void run() throws Ap3Exception
    {
        RemoteHostConfigurationEntry entry;
        
        if(StringUtils.isBlank(id))
        {
            entry = promptForRemoteFromList();
            if(null == entry)
            {
                return;
            }
        }
        else
        {
            entry = getRemoteById(id);

            if(null == entry)
            {
                prompter.showWarning("No remote entry found with id '" + id + "'");
                prompter.showMessage("Run 'ap3 remote list' to list remote entries");
                return;
            }
        }

        try
        {
            deleteRemoteEntry(entry);
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error deleting remote entry", e);
        }

        prompter.showInfo("Remote host entry '" + entry.getId() + "' deleted!");
    }

    
}
