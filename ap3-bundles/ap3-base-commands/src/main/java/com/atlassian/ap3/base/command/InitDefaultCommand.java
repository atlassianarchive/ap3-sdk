package com.atlassian.ap3.base.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.KitConfigurationEntry;
import com.atlassian.ap3.api.kit.KitDescriptorLocator;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.spi.command.BaseAp3Command;
import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.common.base.Function;

import io.airlift.command.Arguments;
import io.airlift.command.Command;

import static com.google.common.collect.Lists.newArrayList;

@Named
@Command(name = "init", description = "Initializes a folder as an Atlassian Plugin")
public class InitDefaultCommand extends BaseAp3Command
{
    private final KitDescriptorLocator descriptorLocator;
    private final Ap3ConfigurationManager configurationManager;
    private final Prompter prompter;

    @Arguments(title = "project type",description = "The type of project to initialize")
    List<String> kitIdArgs = newArrayList();
    
    @Inject
    public InitDefaultCommand(KitDescriptorLocator descriptorLocator, Ap3ConfigurationManager configurationManager, Prompter prompter)
    {
        this.descriptorLocator = descriptorLocator;
        this.configurationManager = configurationManager;
        this.prompter = prompter;
    }

    @Override
    public void run() throws Ap3Exception
    {
        KitDescriptor descriptor = null;
        
        if(!kitIdArgs.isEmpty())
        {
            descriptor = descriptorLocator.getKitDescriptorById(kitIdArgs.get(0));
        }
        
        if(null == descriptor)
        {
            descriptor = prompter.promptForChoice("Choose a project type", descriptorLocator.getAllKitDescriptors(), new Function<KitDescriptor,String>() {
                @Override
                public String apply(KitDescriptor input)
                {
                    return input.getKitId();
                }
            });
        }

        Path projectFile = Paths.get("").resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);
        if(Files.exists(projectFile))
        {
            prompter.showWarning("Current directory is already an AP3 project, aborting...");
            return;
        }

        KitConfigurationEntry kitEntry = new KitConfigurationEntry();
        kitEntry.setId(descriptor.getKitId());

        try
        {
            configurationManager.saveProjectEntry(kitEntry,Paths.get(""));
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error initializing project!", e);
        }
        
        prompter.showInfo("AP3 project successfully initialized");

    }
}
