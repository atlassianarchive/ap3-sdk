package com.atlassian.ap3.base.command;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.DefaultRemoteConfigurationEntry;
import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.api.config.RemotesConfigurationHelper;
import com.atlassian.ap3.spi.command.AbstractConfigCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "remote-host", description = "Configures the default remote host")
public class DefaultRemoteConfigCommand extends AbstractConfigCommand<DefaultRemoteConfigurationEntry>
{
    @Option(name = {"-i", "--id"}, title = "ID", description = "The id of the remote host entry")
    private String id;
    
    private final RemotesConfigurationHelper remotesHelper;

    @Inject
    public DefaultRemoteConfigCommand(RemotesConfigurationHelper remotesHelper)
    {
        this.remotesHelper = remotesHelper;
    }

    @Override
    protected boolean isGlobal()
    {
        return false;
    }

    @Override
    protected DefaultRemoteConfigurationEntry getUpdatedConfigurationEntry() throws Ap3Exception
    {
        DefaultRemoteConfigurationEntry config = getCurrentConfigurationEntry(getEntryType());

        if(null == config)
        {
            config = new DefaultRemoteConfigurationEntry();
        }

        RemoteHostConfigurationEntry entry;
        
        if(StringUtils.isBlank(id))
        {
            entry = remotesHelper.promptForRemoteFromList();
        }
        else
        {
            entry = remotesHelper.getRemoteById(id);
            if(null == entry)
            {
                prompter.showWarning("No remote entry found with id '" + id + "'");
                prompter.showMessage("Run 'ap3 remote list' to list remote entries");
            }
        }
        
        if(null != entry)
        {
            config.setId(entry.getId());
        }
        
        return config;
    }
    
}
