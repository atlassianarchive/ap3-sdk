package com.atlassian.ap3.base.command;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.ContainerResolver;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.KitConfigurationEntry;
import com.atlassian.ap3.api.config.RemoteHostConfigurationEntry;
import com.atlassian.ap3.api.config.RemotesConfigurationHelper;
import com.atlassian.ap3.api.kit.ProjectPackage;
import com.atlassian.ap3.api.kit.ProjectPackagerLocator;
import com.atlassian.ap3.api.maven.MavenPomHelper;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.base.config.BrowserConfigurationEntry;
import com.atlassian.ap3.base.product.P3ContainerStarter;
import com.atlassian.ap3.base.product.P3StartupContext;
import com.atlassian.ap3.spi.command.AbstractStartCommand;
import com.atlassian.ap3.spi.kit.ProjectPackager;
import com.atlassian.localtunnel.DefaultLocalTunnelFactory;
import com.atlassian.localtunnel.LocalTunnel;
import com.atlassian.localtunnel.LocalTunnelFactory;

import com.google.common.collect.ImmutableSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;

import io.airlift.command.Command;
import io.airlift.command.Option;

import static com.google.common.collect.Sets.newHashSet;

@Named
@Command(name = "p3", description = "Starts the Plugins 3 container")
public class P3ContainerStartCommand extends AbstractStartCommand
{
    public static final Set<URI> AUTOREGISTER_HOSTS = ImmutableSet.of(
            URI.create("http://localhost:1990/confluence"),
            URI.create("http://localhost:2990/jira"),
            URI.create("http://localhost:5990/refapp"));

    public static final Pattern HOST_PATTERN = Pattern.compile("([^:]+)[:]([^@]+)[@](https://|http://)?([^:/]+)([:]([0-9]+))?([/](.*))?");

    @Option(name = {"-f", "--force"}, title = "Force Update", description = "forces a container update")
    private boolean forceUpdate = false;

    @Option(name = {"-V", "--version"}, title = "Version", description = "The container version to use")
    private String version = "";

    @Option(name = {"-v"}, title = "Verbosity (-v | -vv | -vvv)", description = "Set the logging verbosity")
    private boolean verbosity1;

    @Option(name = {"-vv"}, hidden = true)
    private boolean verbosity2;

    @Option(name = {"-vvv"}, hidden = true)
    private boolean verbosity3;

    @Option(name = {"-r", "--remote-host-id"}, title = "Remote Host ID", description = "The id of the remote host to register with")
    private String remoteId = "";

    @Option(name = {"-rh", "--remote-host-string"}, title = "Remote Host String", description = "ad-hoc remote host string: user:pass@host[:port][/context]")
    private String remoteHostString = "";

    @Option(name = {"-th", "--tunnel-host"}, title = "Local Tunnel Host", description = "The host for the localtunnel service (default = lt39.info:80)")
    private String tunnelHost = "lt39.info:80";

    @Option(name = {"-nt", "--no-tunnel"}, title = "No Local Tunnel", description = "shuts off localtunnel support")
    private boolean noTunnel = false;

    @Option(name = {"-pm", "--production-mode"}, title = "Production Mode", description = "dev.mode false, no tunneling, no resource mappings")
    private boolean productionMode = false;

    @Option(name = {"-lb", "--launch-browser"}, title = "Launch Browser Flag", description = "a sticky flag to turn on/off launching a browser tab [true|false]",arity = 1)
    private boolean launchBrowser = true;

    private final Prompter prompter;

    private final Ap3ConfigurationManager configurationManager;

    private final ProjectPackagerLocator packagerLocator;

    private final ContainerResolver containerResolver;

    private final MavenPomHelper pomHelper;
    
    private final P3ContainerStarter containerStarter;

    private final RemotesConfigurationHelper remotesHelper;

    @Inject
    public P3ContainerStartCommand(Prompter prompter, Ap3ConfigurationManager configurationManager, ProjectPackagerLocator packagerLocator, ContainerResolver containerResolver, MavenPomHelper pomHelper, P3ContainerStarter containerStarter, RemotesConfigurationHelper remotesHelper)
    {
        this.prompter = prompter;
        this.configurationManager = configurationManager;
        this.packagerLocator = packagerLocator;
        this.containerResolver = containerResolver;
        this.containerStarter = containerStarter;
        this.pomHelper = pomHelper;
        this.remotesHelper = remotesHelper;
    }

    @Override
    public void run() throws Ap3Exception
    {
        Path projectRoot = Paths.get("");

        if (!checkProject(projectRoot))
        {
            return;
        }

        Integer p3port = (port > 0) ? port : 8000;
        
        Pair<Path,String> containerJarAndVersion = getContainerJarAndVersion(version, projectRoot);
        ProjectPackage pluginPackage = packageProject(projectRoot, containerJarAndVersion.getLeft(), containerJarAndVersion.getRight());

        P3StartupContext ctx = createContext(p3port,pluginPackage,containerJarAndVersion,productionMode,launchBrowser);
        
        if (null == pluginPackage.getJarFile())
        {
            prompter.showError("Could not package/find a project jar, aborting...");
            return;
        }

        if(!productionMode)
        {
            Pair<RemoteHostConfigurationEntry,Boolean> remoteAndLocalFlag = getRemoteHost(projectRoot,remoteId,remoteHostString);
            RemoteHostConfigurationEntry remoteHost = remoteAndLocalFlag.getLeft();
            boolean remoteIsLocal = remoteAndLocalFlag.getRight();
            
            if(null != remoteHost)
            {
                ctx.setRemoteHost(remoteHost);
                ctx.setBrowserLaunchUrl(remoteHost.getUrl());   
            }

            if (!remoteIsLocal && !noTunnel)
            {
                prompter.showMessage("Starting local tunnel...");
                startLocalTunnel(ctx, tunnelHost);
            }
        }
        
        containerStarter.start(ctx);
    }

    private void startLocalTunnel(P3StartupContext ctx, String tunnelHost) throws Ap3Exception
    {
        try
        {
            LocalTunnelFactory tunnelFactory = new DefaultLocalTunnelFactory();
            final LocalTunnel tunnel = tunnelFactory.create(ctx.getPort(),tunnelHost);

            tunnel.start();

            if(tunnel.isStarted())
            {
                ctx.setMountBaseUrl(tunnel.getRemoteHost());

                Runtime.getRuntime().addShutdownHook(new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            prompter.showMessage("shutting down localtunnel...");
                            
                            //TODO: figure out how to get around IOException when closing
                            //tunnel.stop();
                        }
                        catch (PrompterException e)
                        {
                            //ignore
                        }
                    }
                });
            }

        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error starting local tunnel", e);
        }
    }

    private Pair<RemoteHostConfigurationEntry,Boolean> getRemoteHost(Path projectRoot, String remoteId, String hostString) throws Ap3Exception
    {
        RemoteHostConfigurationEntry host = null;
        Boolean isLocal = false;
        
        if(StringUtils.isBlank(remoteId) && StringUtils.isBlank(hostString))
        {
            Set<URI> localProducts = findLocalProducts();
    
            if(!localProducts.isEmpty())
            {
                if(localProducts.size() > 1)
                {
                    Set<String> choices = new HashSet<>();
                    for(URI uri : localProducts)
                    {
                        choices.add(uri.toString());
                    }
                    choices.add("none");
                    
                    prompter.showWarning("We found multiple local products running");
                    String hostUrl = prompter.promptForChoice("Please choose which product to register your plugin with:",choices);
                    
                    if(!"none".equals(hostUrl) && StringUtils.isNotBlank(hostUrl))
                    {
                        host = new RemoteHostConfigurationEntry();
                        host.setUrl(hostUrl);
                        host.setUsername("admin");
                        host.setPassword("admin");
                        isLocal = true;
                    }
                }
                else
                {
                    host = new RemoteHostConfigurationEntry();
                    host.setUrl(localProducts.iterator().next().toString());
                    host.setUsername("admin");
                    host.setPassword("admin");
                    isLocal = true;
                }
            }
        }
        else if(StringUtils.isNotBlank(hostString))
        {
            Matcher m = HOST_PATTERN.matcher(hostString);
            if(!m.matches())
            {
                throw new Ap3Exception("Invalid remote host string for pattern 'user:pass@host[:port][/context]' : " + hostString);
            }

            String user = m.group(1);
            String pass = m.group(2);
            
            String scheme = m.group(3);
            String hostname = m.group(4);
            String remotePort = m.group(6);
            String contextPath = m.group(7);
            
            StringBuilder sb = new StringBuilder();
            if(StringUtils.isNotBlank(scheme))
            {
                sb.append(scheme);
            }
            
            sb.append(hostname);
            
            if(StringUtils.isNotBlank(remotePort))
            {
                sb.append(":").append(remotePort);
            }
            
            if(StringUtils.isNotBlank(contextPath))
            {
                sb.append(contextPath);
            }

            host = new RemoteHostConfigurationEntry();
            host.setUrl(sb.toString());
            host.setUsername(user);
            host.setPassword(pass);
            
        }
        
        if(null == host)
        {
            try 
            {
                host = remotesHelper.findOrPromptForRemoteHost(projectRoot,remoteId,"No local products are running and no remote hosts are configured!");
            }
            catch (IOException e)
            {
                throw new Ap3Exception("Unable to get remote host", e);
            }
            
        }
        
        return Pair.of(host,isLocal);
    }

    private P3StartupContext createContext(Integer p3port, ProjectPackage pluginPackage, Pair<Path, String> containerJarAndVersion, boolean productionMode, boolean doBrowserLaunch)
    {
        Integer dPort = (debugPort > 0) ? debugPort : 5004;
        
        P3StartupContext ctx = new P3StartupContext(p3port,pluginPackage,containerJarAndVersion.getLeft(),containerJarAndVersion.getRight());
        ctx.setDebug(debug);
        ctx.setDebugSuspend(debugSuspend);
        ctx.setDebugPort(dPort);
        ctx.setProductionMode(productionMode);

        String vFlag = "";
        if (verbosity1)
        {
            vFlag = "-v";
        }
        if (verbosity2)
        {
            vFlag = "-vv";
        }
        if (verbosity3)
        {
            vFlag = "-vv";
        }

        ctx.setVerbosityFlag(vFlag);
        ctx.setExtraJvmArgs(getExtraJvmArgs());

        List<String> argList = Arrays.asList(getPassedInArgs());
        
        if(argList.contains("-lb") || argList.contains("--launch-browser"))
        {
            ctx.setLaunchBrowser(doBrowserLaunch);
            BrowserConfigurationEntry newBrowserConfig = new BrowserConfigurationEntry();
            newBrowserConfig.setLaunch(doBrowserLaunch);
            try
            {
                configurationManager.saveProjectEntry(newBrowserConfig,pluginPackage.getProjectRoot());
            }
            catch (IOException e)
            {
                //ignore
            }

        }
        else
        {
            BrowserConfigurationEntry browserConfig = configurationManager.getProjectConfigurationEntry(BrowserConfigurationEntry.class,pluginPackage.getProjectRoot());
            if(null == browserConfig)
            {
                browserConfig = configurationManager.getGlobalConfigurationEntry(BrowserConfigurationEntry.class);
            }
            
            if(null != browserConfig)
            {
                ctx.setLaunchBrowser(browserConfig.getLaunch());
            }
            else
            {
                ctx.setLaunchBrowser(doBrowserLaunch);
            }
        }
        return ctx;
    }

    private Pair<Path,String> getContainerJarAndVersion(String passedVersion, Path projectRoot) throws Ap3Exception
    {
        String containerVersion = getContainerVersion(passedVersion, projectRoot);
        Path containerJar = null;
        try
        {
            containerJar = containerResolver.getContainerJar(containerVersion,forceUpdate);

            if (null == containerJar || Files.notExists(containerJar))
            {
                throw new Ap3Exception("Couldn't find a P3 container jar to start!");
            }
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error downloading container", e);
        }
        
        return Pair.of(containerJar,containerVersion);
    }

    private String getContainerVersion(String passedInVersion, Path projectRoot) throws Ap3Exception
    {
        if (StringUtils.isNotBlank(passedInVersion) && !"LATEST".equals(passedInVersion))
        {
            return passedInVersion;
        }

        String finalVersion = "";
        Path pomxml = projectRoot.resolve("pom.xml");

        try
        {
            System.out.println("Resolving container version...");
            String latestVersion = containerResolver.getLatestContainerVersion(true);
            
            if (Files.exists(pomxml))
            {
                finalVersion = pomHelper.getP3VersionProperty(pomxml);
            }

            if (StringUtils.isBlank(finalVersion) || "LATEST".equals(passedInVersion))
            {
                finalVersion = latestVersion;
            }
            else if(StringUtils.isNotBlank(finalVersion))
            {
                GenericVersionScheme scheme = new GenericVersionScheme();
                try 
                {
                    Version finalGV = scheme.parseVersion(finalVersion);
                    Version latestGV = scheme.parseVersion(latestVersion);
                    if(finalGV.compareTo(latestGV) < 0)
                    {
                        prompter.showWarning("The container version in your pom (" + finalVersion + ") is out of date.");
                        prompter.showWarning("The latest container version is: " + latestVersion);
                        /*
                        int sec = 5;
                        while(sec > 0)
                        {
                            prompter.replaceMessage("Staring container with pom version in " + sec + " seconds");
                            Thread.sleep(1000);
                            sec--;
                        }
                        prompter.replaceMessage("");
                        */
                    }
                }
                catch (InvalidVersionSpecificationException e)
                {
                    //ignore
                }
            }
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error getting container version", e);
        }

        if (StringUtils.isBlank(finalVersion))
        {
            finalVersion = "LATEST";
        }

        return finalVersion;
    }

    private Set<URI> findLocalProducts() throws PrompterException
    {
        Set<URI> found = newHashSet();
        prompter.showMessage("Looking for local products...");
        for (URI host : AUTOREGISTER_HOSTS)
        {
            Socket socket = null;
            try
            {
                socket = new Socket(host.getHost(), host.getPort());
                found.add(host);
            }
            catch (UnknownHostException e)
            {
                throw new RuntimeException("Not possible", e);
            }
            catch (IOException e)
            {
                // ignore, and try another
            }
            finally
            {
                try
                {
                    if (socket != null)
                    {
                        socket.close();
                    }
                }
                catch (IOException e)
                {
                    // ignore
                }
            }
        }
        return found;
    }

    private ProjectPackager getPackager(Path projectRoot) throws Ap3Exception
    {
        ProjectPackager packager = null;
        KitConfigurationEntry kitEntry = configurationManager.getProjectConfigurationEntry(KitConfigurationEntry.class, projectRoot);

        if (null != kitEntry)
        {
            packager = packagerLocator.getPackager(kitEntry.getId());
        }

        if (null == packager)
        {
            prompter.showError("Could not find a project packager compatible with this project, aborting...");
            return null;
        }

        return packager;
    }

    private ProjectPackage packageProject(Path projectRoot, Path containerJar, String containerVersion) throws Ap3Exception
    {
        ProjectPackage projectPackage = null;
        ProjectPackager packager = getPackager(projectRoot);

        try
        {
            projectPackage = packager.packageProject(projectRoot, containerJar, containerVersion, getExtraJvmArgs());
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error packaging project", e);
        }

        return projectPackage;
    }

    private boolean checkProject(Path projectRoot) throws PrompterException
    {
        Path projectFile = projectRoot.resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);
        if (Files.notExists(projectFile))
        {
            prompter.showError("Current directory is not a valid AP3 project.");
            prompter.showWarning("Either re-run this command with the -g|--global flag, or run: ap3 init");
            return false;
        }

        return true;
    }

}
