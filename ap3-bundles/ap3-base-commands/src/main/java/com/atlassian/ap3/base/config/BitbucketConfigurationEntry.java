package com.atlassian.ap3.base.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("bitbucket")
public class BitbucketConfigurationEntry
{
    private String username;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
