package com.atlassian.ap3.base.product;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.OAuthConfigurationEntry;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.reload.DescriptorFileWatcher;
import com.atlassian.ap3.api.util.*;
import com.atlassian.ap3.home.HomeLocator;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

@Named
@Singleton
public class P3ContainerStarter
{
    private final HomeLocator homeLocator;
    private final Ap3ConfigurationManager configurationManager;
    private final AppRegistrar appRegistrar;
    private final DescriptorFileWatcher descriptorWatcher;
    private final Prompter prompter;

    @Inject
    public P3ContainerStarter(HomeLocator homeLocator, Ap3ConfigurationManager configurationManager, AppRegistrar appRegistrar, DescriptorFileWatcher descriptorWatcher, Prompter prompter)
    {
        this.homeLocator = homeLocator;
        this.configurationManager = configurationManager;
        this.appRegistrar = appRegistrar;
        this.descriptorWatcher = descriptorWatcher;
        this.prompter = prompter;
    }

    public void start(P3StartupContext ctx) throws Ap3Exception
    {
        try
        {
            List<String> commandLine = getCommands(ctx);
            ProcessBuilder processBuilder = new ProcessBuilder(commandLine);
            Path containerDir = createContainerTempDir();

            System.out.println("Starting P3 Container version " + ctx.getContainerVersion());
            System.out.println("Container working dir is: " + containerDir.toAbsolutePath().toString());
            System.out.println("running p3 with: " + StringUtils.join(commandLine, " "));

            // execute the process in the specified buildRoot
            processBuilder.directory(containerDir.toFile());
            processBuilder.redirectErrorStream(true);

            exportOAuth(ctx.getProjectRoot(), processBuilder);

            final Process process = processBuilder.start();
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        IOUtils.copy(process.getInputStream(), System.out);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }).start();

            pingRepeatedly(ctx.getLocalAppUrl());

            if(!ctx.isProductionMode())
            {
                registerApp(ctx);
            }
            
            if(ctx.getLaunchBrowser())
            {
                LaunchBrowser.launch(ctx.getBrowserLaunchUrl());
            }

            process.waitFor();
        }
        catch (InterruptedException | IOException e)
        {
            throw new Ap3Exception("Error running container!", e);
        }
    }

    private Path createContainerTempDir() throws IOException
    {
        SecureRandom random = new SecureRandom();
        long n = random.nextLong();
        n = (n == Long.MIN_VALUE) ? 0 : Math.abs(n);

        final Path tmpdir = homeLocator.getContainerDirectory().resolve("p3-work").resolve("p3container" + Long.toString(n));
        Files.createDirectories(tmpdir);

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    if (tmpdir != null && Files.exists(tmpdir))
                    {
                        System.out.println("removing tmp dir: " + tmpdir.toAbsolutePath().toString());
                        NIOFileUtils.cleanDirectory(tmpdir);

                        Files.deleteIfExists(tmpdir);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        return tmpdir;
    }

    private void exportOAuth(Path projectRoot, ProcessBuilder processBuilder) throws IOException
    {
        OAuthConfigurationEntry oauthConfig = configurationManager.getProjectConfigurationEntry(OAuthConfigurationEntry.class, projectRoot);
        if (null != oauthConfig)
        {
            String pvtKey = oauthConfig.getPvtkey();

            if (StringUtils.isNotBlank(pvtKey))
            {
                processBuilder.environment().put("OAUTH_LOCAL_PRIVATE_KEY", pvtKey);
            }
        }
    }

    private List<String> getCommands(P3StartupContext ctx)
    {
        List<String> args = getJavaArgs(ctx);
        Path pluginJar = ctx.getPluginJar();

        String javacmd = System.getProperty("ap3.javacmd");
        if (StringUtils.isBlank(javacmd))
        {
            javacmd = "java";
        }

        ImmutableList.Builder<String> commandsBuilder = ImmutableList.builder();
        commandsBuilder.add(javacmd)
                       .addAll(args)
                       .add("-jar")
                       .add(ctx.getContainerJar().toAbsolutePath().toString());

        if (null != pluginJar && Files.exists(pluginJar))
        {
            commandsBuilder.add(pluginJar.toAbsolutePath().toString());
        }

        addContainerArgs(ctx, commandsBuilder);

        return commandsBuilder.build();
    }

    private List<String> getJavaArgs(P3StartupContext ctx)
    {
        ImmutableList.Builder<String> argsBuilder = ImmutableList.builder();

        Path dumpFile = Paths.get(System.getProperty("java.io.tmpdir")).resolve("p3container.hprof");
        try
        {
            Files.deleteIfExists(dumpFile);
        }
        catch (IOException e)
        {
            //just ignore it
        }

        if (ctx.isDebug())
        {
            String suspend = "n";
            if (ctx.isDebugSuspend())
            {
                suspend = "y";
            }

            argsBuilder.add("-Xdebug")
                       .add("-Xrunjdwp:transport=dt_socket,server=y,suspend=" + suspend + ",address=" + ctx.getDebugPort());
        }

        argsBuilder.add("-Xmx512m")
                   .add("-XX:MaxPermSize=256m")
                   .add("-XX:+HeapDumpOnOutOfMemoryError")
                   .add("-XX:HeapDumpPath=" + dumpFile.toAbsolutePath().toString());
        if(!ctx.isProductionMode())
        {
            argsBuilder.add("-Dplugin.resource.directories=" + ctx.getProjectResources().toAbsolutePath().toString())
                       .add("-Datlassian.dev.mode=true");
        }
        else
        {
            argsBuilder.add("-Datlassian.dev.mode=false");
        }

        argsBuilder.add("-DlocalBaseUrl=" + ctx.getMountBaseUrl());

        return JVMArgsUtil.mergeToList(argsBuilder.build(), ctx.getExtraJvmArgs());
    }

    private void addContainerArgs(P3StartupContext ctx, ImmutableList.Builder<String> commandsBuilder)
    {
        if (ctx.getPort() > 0)
        {
            commandsBuilder.add("-p").add(ctx.getPort().toString());
        }

        if (StringUtils.isNotBlank(ctx.getVerbosityFlag()))
        {
            commandsBuilder.add(ctx.getVerbosityFlag());
        }
    }

    private void pingRepeatedly(String url) throws IOException
    {

        int timeout = 1000 * 60 * 10;
        final long end = System.nanoTime() + TimeUnit.MILLISECONDS.toNanos(timeout);
        boolean interrupted = false;
        boolean success = false;
        String lastMessage = "";

        // keep retrieving from the url until a good response is returned, under a time limit.
        while (!success && !interrupted && System.nanoTime() < end)
        {
            HttpURLConnection connection = null;
            try
            {
                URL urlToPing = new URL(url);
                connection = (HttpURLConnection) urlToPing.openConnection();
                int response = connection.getResponseCode();
                // Tomcat returns 404 until the webapp is up
                lastMessage = "Last response code is " + response;
                success = response < 400;
            }
            catch (IOException e)
            {
                lastMessage = e.getMessage();
                success = false;
            }
            finally
            {
                if (connection != null)
                {
                    try
                    {
                        connection.getInputStream().close();
                    }
                    catch (IOException e)
                    {
                        // Don't do anything
                    }
                }
            }

            if (!success)
            {
                //getLog().debug("Waiting for " + url);
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                    interrupted = true;
                    break;
                }
            }
        }

        if (!success)
        {
            throw new IOException(String.format("The container didn't start after %ds at %s. %s", TimeUnit.MILLISECONDS.toSeconds(timeout), url, lastMessage));
        }
    }

    private void registerApp(P3StartupContext ctx) throws IOException, PrompterException
    {
        if (null == ctx.getRemoteHost())
        {
            return;
        }

        try
        {
            final URI hostUri = new URI(ctx.getRemoteHost().getUrl());
            final AppRegistrationDetails regDetails = new AppRegistrationDetails(hostUri, ctx.getPluginKey(), ctx.getMountBaseUrl(), ctx.getRemoteHost().getUsername(), ctx.getRemoteHost().getPassword());

            prompter.showMessage("registering app with remote host ["+ ctx.getRemoteHost().getId() + "] " + ctx.getRemoteHost().getUrl() + " ...");
            appRegistrar.registerApp(regDetails);

            descriptorWatcher.start(regDetails, ctx.getProjectResources());

            Runtime.getRuntime().addShutdownHook(new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        prompter.showMessage("uninstalling app from remote host...");
                        descriptorWatcher.stop(regDetails);
                        appRegistrar.unregisterApp(regDetails);
                    }
                    catch (IOException | PrompterException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (URISyntaxException e)
        {
            prompter.showError("Unable to register app with host");
            e.printStackTrace();
        }
    }

}
