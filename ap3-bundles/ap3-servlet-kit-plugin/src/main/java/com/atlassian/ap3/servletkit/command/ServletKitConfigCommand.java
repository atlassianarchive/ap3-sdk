package com.atlassian.ap3.servletkit.command;

import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.servletkit.config.ServletKitConfigurationEntry;
import com.atlassian.ap3.spi.command.AbstractConfigCommand;

import com.google.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "servlet-kit", description = "Configures Servlet Kit projects")
public class ServletKitConfigCommand extends AbstractConfigCommand<ServletKitConfigurationEntry>
{
    private final Prompter prompter;

    @Option(name = {"-bc", "--build-command"}, title = "Build Command", description = "The command to run to package a project")
    private String buildCommand;

    @Inject
    public ServletKitConfigCommand(Prompter prompter)
    {
        this.prompter = prompter;
    }
    @Override
    protected ServletKitConfigurationEntry getUpdatedConfigurationEntry() throws Ap3Exception
    {
        ServletKitConfigurationEntry config = getCurrentConfigurationEntry(getEntryType());

        if(null == config)
        {
            config = new ServletKitConfigurationEntry();
        }

        if(null == buildCommand)
        {
            try
            {
                String bc;
                if(StringUtils.isNotBlank(config.getBuildCommand()))
                {
                    bc = prompter.prompt("Enter your build command",config.getBuildCommand());
                }
                else
                {
                    bc = prompter.prompt("Enter your build command");
                }

                config.setBuildCommand(bc);

            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("unable to prompt for bitbucket username",e);
            }

        }
        else
        {
            config.setBuildCommand(buildCommand);
        }

        return config;
    }
}
