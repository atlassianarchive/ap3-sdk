package com.atlassian.ap3.servletkit.command;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.JarAndVersion;
import com.atlassian.ap3.api.JarResolver;
import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.template.PluginIdentifier;
import com.atlassian.ap3.api.template.TemplateInfo;
import com.atlassian.ap3.api.util.NamingUtil;
import com.atlassian.ap3.servletkit.ServletKitDescriptor;
import com.atlassian.ap3.spi.command.AbstractProjectCreatorCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@RequiresKit(ServletKitDescriptor.class)
@Command(name = "servlet-kit", description = "Creates a new servlet-kit plugin")
public class NewServletKitCommand extends AbstractProjectCreatorCommand
{
    @Option(name = {"-g", "--groupId"}, title = "Group ID / Package", description = "The group id / java package for your new plugin")
    String groupId;

    @Option(name = {"-ns", "--namespace"}, title = "Namespace", description = "The namespace for your new plugin")
    String namespace;

    private final JarResolver jarResolver;

    @Inject
    public NewServletKitCommand(JarResolver jarResolver)
    {
        this.jarResolver = jarResolver;
    }

    @Override
    protected Map<String, Object> createContext(TemplateInfo templateInfo, PluginIdentifier pluginId) throws Ap3Exception
    {
        Map<String,Object> context = new HashMap<>();
        String packageName = validateGroupId();

        context.put("javaPackage",packageName);
        context.put("groupId",packageName);
        context.put("packageFolder",NamingUtil.packageToPath(packageName));
        
        String ns = validateNamespace(promptForNamespaceIfNeeded());
        context.put("namespace",ns);

        JarAndVersion jav = jarResolver.resolveLatestArtifact("com.atlassian.pluginkit", "servlet-kit");

        if(null != jav)
        {
            context.put("latestServletKitVersion",jav.getVersion());
        }
        
        return context;
    }

    private String validateGroupId() throws Ap3Exception
    {
        String packageName = promptForGroupIdIfNeeded();
                    
        if(!NamingUtil.isValidPackageName(packageName))
        {
            packageName = promptForValidPackage();
        }
        
        return packageName;
    }

    private String promptForValidPackage() throws Ap3Exception
    {
        String packageName = "";
        try
        {
            packageName = prompter.promptNotBlank("Enter a valid package name");
            
            if(!NamingUtil.isValidPackageName(packageName))
            {
                packageName = promptForValidPackage();
            }
        }
        catch (PrompterException e)
        {
            throw new Ap3Exception("Error prompting for package", e);
        }
        
        return packageName;
    }

    private String promptForGroupIdIfNeeded() throws Ap3Exception
    {
        if (StringUtils.isBlank(groupId))
        {
            try
            {
                groupId = prompter.promptNotBlank("Enter a Group Id / Package name");
            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("Error prompting for groupId", e);
            }
        }
        
        return groupId;
    }

    private String validateNamespace(String ns) throws Ap3Exception
    {
        boolean isValid = !StringUtils.containsWhitespace(ns);

        if(isValid)
        {
            try
            {
                Paths.get(ns);
            }
            catch (InvalidPathException e)
            {
                isValid = false;
            }
        }

        if(!isValid)
        {
            ns = validateNamespace(prompter.promptNotBlank("Enter a valid namespace"));
        }

        return ns;
    }

    private String promptForNamespaceIfNeeded() throws Ap3Exception
    {
        if (StringUtils.isBlank(namespace))
        {
            try
            {
                namespace = prompter.promptNotBlank("Enter a namespace");
            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("Error prompting for namespace", e);
            }
        }

        return namespace;
    }
}
