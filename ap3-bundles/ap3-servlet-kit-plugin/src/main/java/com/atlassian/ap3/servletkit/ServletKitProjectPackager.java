package com.atlassian.ap3.servletkit;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.kit.MavenPackageBuilder;
import com.atlassian.ap3.api.kit.ProjectPackage;
import com.atlassian.ap3.api.util.JVMArgsUtil;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.servletkit.config.ServletKitConfigurationEntry;
import com.atlassian.ap3.spi.kit.ProjectPackager;
import com.atlassian.plugin.remotable.descriptor.PolyglotDescriptorAccessor;

import org.apache.commons.lang3.StringUtils;

import static com.google.common.collect.Lists.newArrayList;

@Named
@Singleton
@RequiresKit(ServletKitDescriptor.class)
public class ServletKitProjectPackager implements ProjectPackager
{
    private static final String DEFAULT_BUILD_COMMAND = "mvn package";
    
    private final Ap3ConfigurationManager configurationManager;
    private final MavenPackageBuilder mavenPackageBuilder;

    @Inject
    public ServletKitProjectPackager(Ap3ConfigurationManager configurationManager, MavenPackageBuilder mavenPackageBuilder)
    {
        this.configurationManager = configurationManager;
        this.mavenPackageBuilder = mavenPackageBuilder;
    }

    @Override
    public ProjectPackage packageProject(Path projectRoot, Path containerJar, String containerVersion, List<String> jvmArgs) throws IOException
    {
        String buildCommand = getBuildCommand(projectRoot);
        
        Path plugin = mavenPackageBuilder.build(projectRoot,buildCommand,jvmArgs);
        Path resourceDir = projectRoot.resolve("src").resolve("main").resolve("resources");
        return new ProjectPackage(projectRoot,resourceDir,getPluginKey(resourceDir),plugin);
    }

    private String getPluginKey(Path resourceDir)
    {
        PolyglotDescriptorAccessor descriptorAccessor = new PolyglotDescriptorAccessor(resourceDir.toAbsolutePath().toFile());

        return descriptorAccessor.getDescriptor().getRootElement().attributeValue("key");
    }

    private String getBuildCommand(Path projectRoot)
    {
        ServletKitConfigurationEntry globalConfig = configurationManager.getGlobalConfigurationEntry(ServletKitConfigurationEntry.class);
        ServletKitConfigurationEntry projectConfig = configurationManager.getProjectConfigurationEntry(ServletKitConfigurationEntry.class, projectRoot);
                
        String buildCommand = DEFAULT_BUILD_COMMAND;
        
        if(null != globalConfig && StringUtils.isNotBlank(globalConfig.getBuildCommand()))
        {
            buildCommand = globalConfig.getBuildCommand();
        }

        if(null != projectConfig && StringUtils.isNotBlank(projectConfig.getBuildCommand()))
        {
            buildCommand = projectConfig.getBuildCommand();
        }
        
        return buildCommand;
    }
}
