package com.atlassian.ap3.home;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

@Named
@Singleton
public class HomeLocatorImpl implements HomeLocator
{
    private Path homeDir;
    private Path libDir;
    private Path pluginsDir;
    private Path configDir;
    private Path templateDir;
    private Path containerDir;
    private Path userHome;
    
    @Override
    public Path getHomeDirectory() throws IOException
    {
        if(null == homeDir)
        {
            String ap3Home = System.getProperty(AP3_HOME_PROP);
            if(StringUtils.isBlank(ap3Home))
            {
                throw new IOException("Could not find AP3 home directory!\nPlease re-install the sdk.");
            }

            Path tmpHome = Paths.get(ap3Home);
            if(Files.notExists(tmpHome))
            {
                throw new IOException("Could not find AP3 home directory: " + tmpHome.toString() + "\nPlease re-install the sdk.");
            }
            
            homeDir = tmpHome;
        }
        
        return homeDir;
    }

    @Override
    public Path getLibDirectory() throws IOException
    {
        if(null == libDir)
        {
            libDir = getHomeDirectory().resolve(AP3_LIB_DIR);
            Files.createDirectories(libDir);
        }

        return libDir;
    }

    @Override
    public Path getPluginsDirectory() throws IOException
    {
        if(null == pluginsDir)
        {
            pluginsDir = getHomeDirectory().resolve(AP3_PLUGINS_DIR);
            Files.createDirectories(pluginsDir);
        }
        
        return pluginsDir;
    }

    @Override
    public Path getConfigDirectory() throws IOException
    {
        if(null == configDir)
        {
            configDir = getUserHomeDirectory().resolve(AP3_CONFIG_DIR);
            Files.createDirectories(configDir);
        }
        
        return configDir;
    }

    @Override
    public Path getConfigTemplateDirectory() throws IOException
    {
        if(null == templateDir)
        {
            templateDir = getConfigDirectory().resolve(AP3_TEMPLATE_DIR);
            //TODO: git clone here instead of force mkdir
            Files.createDirectories(templateDir);
        }

        return templateDir;
    }

    @Override
    public Path getContainerDirectory() throws IOException
    {
        if(null == containerDir)
        {
            containerDir = getConfigDirectory().resolve(AP3_CONTAINER_DIR);
            Files.createDirectories(containerDir);
        }

        return containerDir;
    }

    @Override
    public Path getUserHomeDirectory() throws IOException
    {
        if(null == userHome)
        {
            userHome = Paths.get(System.getProperty("user.home"));
        }
        
        return userHome;
    }
}
