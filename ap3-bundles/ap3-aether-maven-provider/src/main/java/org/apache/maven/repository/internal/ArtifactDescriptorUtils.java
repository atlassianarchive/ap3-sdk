package org.apache.maven.repository.internal;

import org.apache.maven.model.Repository;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.repository.RepositoryPolicy;

/**
 * <strong>Warning:</strong> This is an internal utility class that is only public for technical reasons, it is not part
 * of the public API. In particular, this class can be changed or deleted without prior notice.
 *
 * @author Benjamin Bentmann
 */
public class ArtifactDescriptorUtils
{

    public static Artifact toPomArtifact( Artifact artifact )
    {
        Artifact pomArtifact = artifact;

        if ( pomArtifact.getClassifier().length() > 0 || !"pom".equals( pomArtifact.getExtension() ) )
        {
            pomArtifact =
                    new DefaultArtifact( artifact.getGroupId(), artifact.getArtifactId(), "pom", artifact.getVersion() );
        }

        return pomArtifact;
    }

    public static RemoteRepository toRemoteRepository( Repository repository )
    {
        RemoteRepository result = new RemoteRepository.Builder(repository.getId(), repository.getLayout(), repository.getUrl())
                .setPolicy(toRepositoryPolicy(repository.getSnapshots()))
                .setPolicy(toRepositoryPolicy(repository.getReleases()))
                .build();

        return result;
    }

    public static RepositoryPolicy toRepositoryPolicy( org.apache.maven.model.RepositoryPolicy policy )
    {
        boolean enabled = true;
        String checksums = RepositoryPolicy.CHECKSUM_POLICY_WARN;
        String updates = RepositoryPolicy.UPDATE_POLICY_DAILY;

        if ( policy != null )
        {
            enabled = policy.isEnabled();
            if ( policy.getUpdatePolicy() != null )
            {
                updates = policy.getUpdatePolicy();
            }
            if ( policy.getChecksumPolicy() != null )
            {
                checksums = policy.getChecksumPolicy();
            }
        }

        return new RepositoryPolicy( enabled, updates, checksums );
    }

}
