package com.atlassian.ap3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import javax.imageio.spi.ServiceRegistry;

import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.home.HomeLocatorImpl;

import com.google.common.collect.ImmutableList;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;


/**
 * @since version
 */
public class OSGiLauncher
{
    
    private OSGiLauncher()
    {
    }

    public static Framework launch() throws Exception
    {
        OSGiLauncher launcher = new OSGiLauncher();
        return launcher.startFelix();
    }
    
    public Framework startFelix() throws Exception
    {
        HomeLocator homeLocator = new HomeLocatorImpl();
        Path homeDir = homeLocator.getHomeDirectory();
        Path cacheDir = homeDir.resolve("bundlecache");

        final Map<String, Object> configuration = new HashMap<>();

        configuration.put( Constants.FRAMEWORK_STORAGE_CLEAN, Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT );
        configuration.put( Constants.FRAMEWORK_STORAGE, cacheDir.toAbsolutePath().toString() );
        configuration.put( Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA,"javax.xml.stream; version=1.0,javax.xml.stream.events; version=1.0,javax.xml.stream.util; version=1.0,org.bouncycastle,org.bouncycastle.openssl");
        
        //configuration.put("felix.log.level","4");

        configuration.putAll( (Map)System.getProperties() );

        final FrameworkFactory frameworkFactory = ServiceRegistry.lookupProviders(FrameworkFactory.class).next();
        final Framework framework = frameworkFactory.newFramework( configuration );
        framework.start();

        final BundleContext ctx = framework.getBundleContext();

        final List<Bundle> frameworkBundles = installBundles(homeLocator.getLibDirectory(),ctx);
        final List<Bundle> pluginBundles = installBundles(homeLocator.getPluginsDirectory(),ctx);

        
        for ( final Bundle frameworkBundle : frameworkBundles )
        {
            if ( frameworkBundle.getHeaders().get( Constants.FRAGMENT_HOST ) == null )
            {
                //System.out.println("starting bundle: " + frameworkBundle.getSymbolicName());
                frameworkBundle.start();
            }
        }

        for ( final Bundle pluginBundle : pluginBundles )
        {
            if ( pluginBundle.getHeaders().get( Constants.FRAGMENT_HOST ) == null )
            {
                //System.out.println("starting bundle: " + pluginBundle.getSymbolicName());
                pluginBundle.start();
            }
        }

        return framework;
    }
    
    private List<Bundle> installBundles(Path bundleDir, BundleContext ctx) throws IOException
    {
        final List<Bundle> bundles = new ArrayList<>();

        File[] files = bundleDir.toFile().listFiles();
        if (files != null)
        {
            Arrays.sort(files);
            try 
            {
                for (int i = 0; i < files.length; i++)
                {
                    String fn = files[i].getName();
                    if (!fn.startsWith(".") && !fn.contains("framework") && fn.endsWith(".jar"))
                    {
                        bundles.add( ctx.installBundle( "reference:" + files[i].toURI()) );
                    }
                }
            }
            catch (BundleException e)
            {
                throw new IOException("Error loading bundle", e);
            }
        }
        
        return ImmutableList.copyOf(bundles);
    }
}
