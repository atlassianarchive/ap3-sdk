package com.atlassian.ap3;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import com.google.inject.Injector;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.launch.Framework;

public class Bootstrap
{
    private Map<Class<?>, List<String>> commandMap;
    private Injector injector;
    private Framework felix;

    public static void main(String[] args)
    {
        Bootstrap bootstrap = new Bootstrap();
        try
        {
            bootstrap.launch();
            bootstrap.runCli(args);
            bootstrap.stop();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            bootstrap.stop();
            System.exit(1);
        }
    }

    public void launch() throws Exception
    {
        long startTime = System.currentTimeMillis();

            felix = OSGiLauncher.launch();

            Runtime.getRuntime().addShutdownHook(new Thread("Felix Shutdown Hook") {
                public void run()
                {
                    try
                    {
                        if (felix != null)
                        {
                            felix.stop();
                            felix.waitForStop(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.err.println("Error stopping framework: " + ex);
                    }
                }
            });
    }

    public void runCli(String[] args) throws Exception
    {
        BundleContext ctx = felix.getBundleContext();
            ServiceReference[] refs = ctx.getServiceReferences("com.atlassian.ap3.cli.Ap3Cli", null);
            if (null != refs && refs.length > 0)
            {
                ServiceReference ref = refs[0];
                Object cli = ctx.getService(ref);
                Method m = cli.getClass().getDeclaredMethod("run", String[].class);
                m.invoke(cli, (Object) args);
            }
    }
    
    public void stop()
    {
        if (null != felix)
        {
            try
            {
                felix.stop();
                felix.waitForStop(0);
                System.exit(0);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }
}
